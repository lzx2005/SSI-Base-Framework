package service;

import java.util.List;
import java.util.Map;

public interface BaseService<T> {

  /**
   * 
   * 分页查询数据
   * 
   * @param params
   * @return
   * @throws Exception
   *         List<T>
   * @exception
   * @since 1.0.0
   */
  public List<T> queryPage(Map<String, Object> params) throws Exception;

  /**
   * 
   * 取得分页数据的总条数
   * 
   * @param params
   * @return
   * @throws Exception
   *         long
   * @exception
   * @since 1.0.0
   */
  public long queryPageCount(Map<String, Object> params) throws Exception;

  /**
   * 
   * 根据ID获取数据
   * 
   * @param id
   * @return
   * @throws Exception
   *         T
   * @exception
   * @since 1.0.0
   */
  public T queryById(Long id) throws Exception;

  /**
   * 
   * 保存数据
   * 
   * @param target
   * @return
   * @throws Exception
   *         T
   * @exception
   * @since 1.0.0
   */
  public T save(T target) throws Exception;

  /**
   * 
   * 更新数据
   * 
   * @param target
   * @throws Exception
   *         void
   * @exception
   * @since 1.0.0
   */
  public void update(T target) throws Exception;

  /**
   * 
   * 逻辑删除数据，即修改disabled为禁用
   * 
   * @param id
   * @param currentUserId
   *        当前登录用户ID
   * @throws Exception
   *         void
   * @exception
   * @since 1.0.0
   */
  public void delete(Long id, Long currentUserId) throws Exception;

  /**
   * 得到真正的PO class
   * 
   * @return Class<T>
   * @exception
   * @since 1.0.0
   */
  public Class<T> getClazz();
}
