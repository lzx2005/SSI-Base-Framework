package util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.DefaultPropertiesPersister;
import org.springframework.util.PropertiesPersister;

/**
 * Properties Util函数.<br>
 * 
 * 从classpath读取配置文件，示例用法：<br>
 * 
 * String loginConfigFile = "classpath:/properties/logon.properties"; <br>
 * Properties p = PropertiesUtils.loadProperties(loginConfigFile);
 * 
 * @author DingZhanhe
 * @version 1.0, 2012-12-20 16:06:15
 * @since 1.0
 */
public class PropertiesUtils {

  private static final String DEFAULT_ENCODING = "UTF-8";

  private static PropertiesPersister propertiesPersister = new DefaultPropertiesPersister();
  private static ResourceLoader resourceLoader = new DefaultResourceLoader();

  /**
   * 载入多个properties文件, 相同的属性在最后载入的文件中的值将会覆盖之前的载入. 文件路径使用Spring Resource格式,
   * 文件编码使用UTF-8.
   * 
   * @see org.springframework.beans.factory.config.PropertyPlaceholderConfigurer
   */
  public static Properties loadProperties(String... resourcesPaths) throws IOException {
    Properties props = new Properties();

    for (String location : resourcesPaths) {
      LoggerUtils.sysConfigDebug("Loading properties file from:" + location);

      InputStream is = null;
      try {
        Resource resource = resourceLoader.getResource(location);
        is = resource.getInputStream();
        propertiesPersister.load(props, new InputStreamReader(is, DEFAULT_ENCODING));
      } catch (IOException ex) {
        LoggerUtils.errorInfo("Could not load properties from classpath:" + location + ": " + ex.getMessage());
      } finally {
        if (is != null) {
          is.close();
        }
      }
    }
    return props;
  }
}
