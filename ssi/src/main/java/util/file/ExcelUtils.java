package util.file;

public class ExcelUtils {
  public static final String EXCEL_VERSION_XLS = "xls";
  public static final String EXCEL_VERSION_XLSX = "xlsx";
  
  public static final String[] PARSEPATTERNS ={"yyyy.MM.dd","yyyy-MM-dd","yyyy/MM/dd"};
}
