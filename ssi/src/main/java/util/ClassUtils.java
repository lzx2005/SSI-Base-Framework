package util;

/**
 * 类的工具类
 * 
 * @author gebing
 * 
 */
public class ClassUtils {

  /**
   * 获取当前方法名
   * 
   * @param throwable
   * @return
   */
  public static String getCurrentMethodName(Throwable throwable) {
    if (throwable == null)
      return null;
    return throwable.getStackTrace()[0].getMethodName();
  }

  /**
   * 获取当前方法名
   * 
   * @param throwable
   * @return
   */
  public static String getCurrentClassName(Throwable throwable) {
    if (throwable == null)
      return null;
    return throwable.getStackTrace()[0].getClassName();
  }

  /**
   * 获取SQLMapId
   * 
   * @param poClass
   *          po对象类
   * @param throwable
   * @return
   */
  public static String getSQLMapId(Class<?> poClass, Throwable throwable) {
    return poClass.getSimpleName() + "." + getCurrentMethodName(throwable);
  }
}
