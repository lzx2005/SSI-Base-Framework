/*
 * @(#)Page.java 2011-10-27
 *
 * Copyright 2011 北龙中网（北京）科技有限责任公司. All rights reserved.
 */
package util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.StringUtils;

/**
 * 分页参数及查询结果封装. 所有序号从1开始.
 * 
 * @author DingZhanhe
 * @version 1.0, 2012-9-26 19:15:55
 * @since 1.0
 */
@XmlRootElement(name = "page")
@XmlType(name = "page")
@XmlAccessorType(XmlAccessType.FIELD)
public class Page<T> {
	// -- 公共变量 --//
	public static final String ASC = "asc";
	public static final String DESC = "desc";

	// -- 分页参数 --//
	protected int pageNo = 1;
	protected int pageSize = -1;
	protected String orderBy = null;
	protected String order = null;
	protected boolean autoCount = false;// 默认为false.

	// -- 返回结果 --//
	protected Collection<T> result = new ArrayList<T>();
	protected long totalCount = -1;
	/** 大翻页一半的页数 */
	protected int tuneSize = 5;

	// -- 构造函数 --//
	public Page() {
	}

	public Page(int pageSize) {
		this.pageSize = pageSize;
	}

	// -- 分页参数访问函数 --//
	/**
	 * 获得当前页的页号,序号从1开始,默认为1.
	 */
	public int getPageNo() {
		if(pageNo < 1)
			pageNo = 1;
		return pageNo;
	}

	/**
	 * 设置当前页的页号,序号从1开始,低于1时自动调整为1.
	 */
	public void setPageNo(final int pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * 返回Page对象自身的setPageNo函数,可用于连续设置。
	 */
	public Page<T> pageNo(final int thePageNo) {
		setPageNo(thePageNo);
		return this;
	}

	/**
	 * 获得每页的记录数量, 默认为-1.
	 */
	public int getPageSize() {
		if(pageSize < 1)
			pageSize = 10;
		return pageSize;
	}

	/**
	 * 设置每页的记录数量.
	 */
	public void setPageSize(final int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 返回Page对象自身的setPageSize函数,可用于连续设置。
	 */
	public Page<T> pageSize(final int thePageSize) {
		setPageSize(thePageSize);
		return this;
	}

	/**
	 * 根据pageNo和pageSize计算当前页第一条记录在总结果集中的位置,序号从1开始.
	 */
	public int getFirst() {
		return ((pageNo - 1) * pageSize) + 1;
	}

	/**
	 * 获得排序字段,无默认值. 多个排序字段时用','分隔.
	 */
	public String getOrderBy() {
		return orderBy;
	}

	/**
	 * 设置排序字段,多个排序字段时用','分隔.
	 */
	public void setOrderBy(final String orderBy) {
		this.orderBy = orderBy;
	}

	/**
	 * 返回Page对象自身的setOrderBy函数,可用于连续设置。
	 */
	public Page<T> orderBy(final String theOrderBy) {
		setOrderBy(theOrderBy);
		return this;
	}

	/**
	 * 获得排序方向, 无默认值.
	 */
	public String getOrder() {
		return order;
	}

	/**
	 * 设置排序方式向.
	 * 
	 * @param order
	 *            可选值为desc或asc,多个排序字段时用','分隔.
	 */
	public void setOrder(final String order) {
		String lowcaseOrder = StringUtils.lowerCase(order);

		// 检查order字符串的合法值
		String[] orders = StringUtils.split(lowcaseOrder, ',');
		for (String orderStr : orders) {
			if (!StringUtils.equals(DESC, orderStr)
					&& !StringUtils.equals(ASC, orderStr)) {
				throw new IllegalArgumentException("排序方向" + orderStr + "不是合法值");
			}
		}

		this.order = lowcaseOrder;
	}

	/**
	 * 返回Page对象自身的setOrder函数,可用于连续设置。
	 */
	public Page<T> order(final String theOrder) {
		setOrder(theOrder);
		return this;
	}

	/**
	 * 是否已设置排序字段,无默认值.
	 */
	public boolean isOrderBySetted() {
		return (StringUtils.isNotBlank(orderBy) && StringUtils
				.isNotBlank(order));
	}

	/**
	 * 获得查询对象时是否先自动执行count查询获取总记录数, 默认为false.
	 */
	public boolean isAutoCount() {
		return autoCount;
	}

	/**
	 * 设置查询对象时是否自动先执行count查询获取总记录数.
	 */
	public void setAutoCount(final boolean autoCount) {
		this.autoCount = autoCount;
	}

	/**
	 * 返回Page对象自身的setAutoCount函数,可用于连续设置。
	 */
	public Page<T> autoCount(final boolean theAutoCount) {
		setAutoCount(theAutoCount);
		return this;
	}

	// -- 访问查询结果函数 --//

	/**
	 * 获得页内的记录列表.
	 */
	public Collection<T> getResult() {
		return result;
	}

	/**
	 * 设置页内的记录列表.
	 */
	public void setResult(final Collection<T> result) {
		this.result = result;
	}

	/**
	 * 获得总记录数, 默认值为-1.
	 */
	public long getTotalCount() {
		return totalCount;
	}

	/**
	 * 设置总记录数.
	 */
	public void setTotalCount(final long totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * 根据pageSize与totalCount计算总页数, 默认值为-1.
	 */
	public long getTotalPages() {
		if (totalCount < 0) {
			return -1;
		}

		int count = (int) totalCount / pageSize;
		if (totalCount % pageSize > 0) {
			count++;
		}
		return count;
	}

	/**
	 * 是否还有下一页.
	 */
	public boolean isHasNext() {
		return (pageNo + 1 <= getTotalPages());
	}

	/**
	 * 取得下页的页号, 序号从1开始. 当前页为尾页时仍返回尾页序号.
	 */
	public int getNextPage() {
		if (isHasNext()) {
			return pageNo + 1;
		} else {
			return pageNo;
		}
	}

	/**
	 * 是否还有上一页.
	 */
	public boolean isHasPre() {
		return (pageNo - 1 >= 1);
	}

	/**
	 * 取得上页的页号, 序号从1开始. 当前页为首页时返回首页序号.
	 */
	public int getPrePage() {
		if (isHasPre()) {
			return pageNo - 1;
		} else {
			return pageNo;
		}
	}

	/**
	 * 模仿阿里巴巴的分页效果。 描述：根据page对象中得当前页和总页数，计算出来以当前页为中心前后各偏移4个数位的pages列表。
	 * example1:如果当前页为6,该方法返回的为{2,3,4,5,6,7,8,9,10},依次类推
	 * example2:如果当前页为1,该方法返回的为{1,2,3,4,5,6,7,8,9}. 目前好像还有些bug，需要进一步优化。
	 * 
	 * @return
	 * @author <a href="mailto:wangxin@knet.cn">wangxin</a>
	 */
	public List<String> getPageList() {
		List<String> pages = new ArrayList<String>();
		int correction = 0;// 偏移量
		for (int i = 4; i > 0; i--) {
			if ((pageNo - i) > 0)
				pages.add(String.valueOf(pageNo - i));
			else
				correction++;
		}
		pages.add(String.valueOf(pageNo));
		for (int i = 1; i < 5 + correction; i++) {
			if ((pageNo + i) <= getTotalPages())
				pages.add(String.valueOf(pageNo + i));
		}
		return pages;
	}

	/**
	 * 得到翻页起始页
	 * 
	 * @return
	 */
	public int getStartTuneNo() {
		int start = pageNo - tuneSize > 0 ? pageNo - tuneSize : 1;
		int end = (int) (getTotalPages() - (pageNo + tuneSize) > 0 ? pageNo
				+ tuneSize : getTotalPages());
		// 已经够页数
		if (end - start >= 2 * tuneSize) {
			return start;
		}
		// 需要补齐的条数
		int x = 2 * tuneSize + 1 - (end - start + 1);
		// 从start补齐
		if (start > 1) {
			return start - x > 1 ? start - x : 1;
		}
		return start;
	}

	/**
	 * 得到翻页结束页
	 * 
	 * @return
	 */
	public int getEndTuneNo() {
		int start = pageNo - tuneSize > 0 ? pageNo - tuneSize : 1;
		int end = (int) (getTotalPages() - (pageNo + tuneSize) > 0 ? pageNo
				+ tuneSize : getTotalPages());
		// 已经够页数
		if (end - start >= 2 * tuneSize) {
			return end;
		}
		// 需要补齐的条数
		int x = 2 * tuneSize + 1 - (end - start + 1);
		// 从end补齐
		if (end < getTotalPages()) {
			return (int) (end + x > getTotalPages() ? getTotalPages() : end + x);
		}
		return end;
	}
}
