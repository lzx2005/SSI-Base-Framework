package util;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * json 解析类，请使用单利
 * 
 * @author DingZhanhe
 * @version 1.0
 * @since 1.0
 */
public class JsonUtil {
	private static Gson gson;
	private final static String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

	static {
		GsonBuilder gsb = new GsonBuilder();
		gsb.setDateFormat(DATE_PATTERN);
		gson = gsb.create();
	}

	/**
	 * 从bean生成json串
	 * 
	 * @param bean
	 * @return
	 */
	public static String toJson(Object bean) {
		return gson.toJson(bean);
	}

	/**
	 * 从bean生成json串
	 * 
	 * @param bean
	 * @param datePattern
	 *            时间格式 类似 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String toJson(Object bean, String datePattern) {
		GsonBuilder gsb = new GsonBuilder();
		gsb.setDateFormat(datePattern);
		return gsb.create().toJson(bean);
	}

	/**
	 * 从json串生成对象
	 * 
	 * @param json
	 * @param clazz
	 * @return
	 */
	public static <T> T fromJson(String json, Class<T> clazz) {
		return (T) gson.fromJson(json, clazz);
	}

	/**
	 * 用于转换map
	 * 
	 * @param json
	 * @param type
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T fromJson(String json, Type type) {
		return (T) gson.fromJson(json, type);
	}

	/**
	 * 从json串生成对象
	 * 
	 * @param json
	 * @param clazz
	 * @param datePattern
	 *            时间格式 类似 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static <T> T fromJson(String json, Class<T> clazz, String datePattern) {
		GsonBuilder gsb = new GsonBuilder();
		gsb.setDateFormat(datePattern);
		return (T) gsb.create().fromJson(json, clazz);
	}

	/**
	 * 用于转换map
	 * 
	 * @param json
	 * @param type
	 * @param datePattern
	 *            时间格式 类似 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T fromJson(String json, Type type, String datePattern) {
		GsonBuilder gsb = new GsonBuilder();
		gsb.setDateFormat(datePattern);
		return (T) gsb.create().fromJson(json, type);
	}

	public static <T> String toJson(List<T> list) {
		return gson.toJson(list);
	}

	public static <T> String toJson(List<T> list, Type type) {

		return gson.toJson(list, type);
	}

}
