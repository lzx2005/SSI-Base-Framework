package util;

import java.util.HashMap;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;

/**
 * @ClassName: JsonResult
 * @Description:
 * @author fanqinghui100@gmail.com
 * @date 2013-5-3 下午8:22:20
 */

/**
 * JSON Data Reslut
 */
public class JsonResult {

    private String type;
    private String message;
    private HashMap<Integer,String> errorMap;
    /** 
	* <p>Title:</p>  
	*/ 
	public JsonResult() {
	}

	/**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message
     *            the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public String toJsonString() {
        try {
            return JSONUtil.serialize(this);
        } catch (JSONException e) {
            e.printStackTrace();
            return "{'failure','" + e.getMessage() + "'}";
        }
    }

	public HashMap<Integer, String> getErrorMap() {
		return errorMap;
	}

	public void setErrorMap(HashMap<Integer, String> errorMap) {
		this.errorMap = errorMap;
	}

}

