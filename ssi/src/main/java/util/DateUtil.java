package util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
  /**
   * 格式化日期
   * 
   * @param dateStr
   *        字符型日期
   * @param format
   *        格式
   * @return 返回日期
   */
  public static Date parseDate(String dateStr, String format) {
    Date date = null;
    try {
      DateFormat df = new SimpleDateFormat(format);
      String dt = dateStr.replaceAll("-", "/");
      if ((!dt.equals("")) && (dt.length() < format.length())) {
        dt += format.substring(dt.length()).replaceAll("[YyMmDdHhSs]", "0");
      }
      date = (Date) df.parse(dt);
    } catch (Exception e) {
    }
    return date;
  }

  public static Date parseDate(String dateStr) {
    return parseDate(dateStr, "yyyy/MM/dd");
  }

  public static Date parseDate(java.sql.Date date) {
    return date;
  }

  public static Date parseSqlDate(Date date) {
    if (date != null)
      return new java.sql.Date(date.getTime());
    else
      return null;
  }

  public static Date parseSqlDate(String dateStr, String format) {
    Date date = parseDate(dateStr, format);
    return parseSqlDate(date);
  }

  public static Date parseSqlDate(String dateStr) {
    return parseSqlDate(dateStr, "yyyy/MM/dd");
  }

  public static Timestamp parseTimestamp(String dateStr, String format) {
    Date date = parseDate(dateStr, format);
    if (date != null) {
      long t = date.getTime();
      return new java.sql.Timestamp(t);
    } else
      return null;
  }

  public static Timestamp parseTimestamp(String dateStr) {
    return parseTimestamp(dateStr, "yyyy/MM/dd  HH:mm:ss");
  }

  /**
   * 格式化输出日期
   * 
   * @param date
   *        日期
   * @param format
   *        格式
   * @return 返回字符型日期
   */
  public static String format(Date date, String format) {
    String result = "";
    try {
      if (date != null) {
        java.text.DateFormat df = new java.text.SimpleDateFormat(format);
        result = df.format(date);
      }
    } catch (Exception e) {
    }
    return result;
  }

  public static String format(Date date) {
    return format(date, "yyyy/MM/dd");
  }

  /**
   * 返回年份
   * 
   * @param date
   *        日期
   * @return 返回年份
   */
  public static int getYear(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.YEAR);
  }

  /**
   * 返回月份
   * 
   * @param date
   *        日期
   * @return 返回月份
   */
  public static int getMonth(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.MONTH) + 1;
  }

  /**
   * 返回日份
   * 
   * @param date
   *        日期
   * @return 返回日份
   */
  public static int getDay(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * 返回小时
   * 
   * @param date
   *        日期
   * @return 返回小时
   */
  public static int getHour(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.HOUR_OF_DAY);
  }

  /**
   * 返回分钟
   * 
   * @param date
   *        日期
   * @return 返回分钟
   */
  public static int getMinute(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.MINUTE);
  }

  /**
   * 返回秒钟
   * 
   * @param date
   *        日期
   * @return 返回秒钟
   */
  public static int getSecond(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.get(Calendar.SECOND);
  }

  /**
   * 返回毫秒
   * 
   * @param date
   *        日期
   * @return 返回毫秒
   */
  public static long getMillis(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    return c.getTimeInMillis();
  }

  /**
   * 返回字符型日期
   * 
   * @param date
   *        日期
   * @return 返回字符型日期
   */
  public static String getDate(Date date) {
    return format(date, "yyyy/MM/dd");
  }

  /**
   * 返回字符型时间
   * 
   * @param date
   *        日期
   * @return 返回字符型时间
   */
  public static String getTime(Date date) {
    return format(date, "HH:mm:ss");
  }

  /**
   * 返回字符型日期时间
   * 
   * @param date
   *        日期
   * @return 返回字符型日期时间
   */
  public static String getDateTime(Date date) {
    return format(date, "yyyy/MM/dd  HH:mm:ss");
  }

  /**
   * 日期相加
   * 
   * @param date
   *        日期
   * @param day
   *        天数
   * @return 返回相加后的日期
   */
  public static Date addDate(Date date, int day) {
    Calendar c = Calendar.getInstance();
    c.setTimeInMillis(getMillis(date) + ((long) day) * 24 * 3600 * 1000);
    return c.getTime();
  }

  /**
   * 日期相减
   * 
   * @param date
   *        日期
   * @param date1
   *        日期
   * @return 返回相减后的日期
   */
  public static int diffDate(Date date, Date date1) {
    return (int) ((getMillis(date) - getMillis(date1)) / (24 * 3600 * 1000));
  }

  // public static void main(String[] args ){
  //
  // Date date=DateUtil.parseSqlDate("2012-10-21");
  //
  // System.out.println(date);
  //
  // }
}
