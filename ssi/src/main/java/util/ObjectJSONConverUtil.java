package util;

import java.lang.reflect.Field;
import java.lang.reflect.Type;

import org.apache.tapestry.json.JSONObject;

/**
 * 
 * ObjectJSONConverUtil　<br>
 * <b>只转换String属性(只提供给Dto与JSON互转)</b><br>
 * 
 * GeBing 2012-10-26 上午11:34:50
 * 
 * @version 1.0.0
 * 
 */
public class ObjectJSONConverUtil<T> {

  /**
   * 
   * Dto对象转JSON<br>
   * <b>只转换String属性(只提供给Dto与JSON互转)</b><br>
   * 
   * @param t
   * @return JSONObject
   * @exception
   * @since 1.0.0
   */
  public JSONObject toJSON(T t) {
    JSONObject json = new JSONObject();
    convert(json, t, t.getClass());
    return json;
  }

  /**
   * 
   * JSON转Dto对象<br>
   * <b>只转换String属性(只提供给Dto与JSON互转)</b><br>
   * 
   * @param json
   * @param t
   *          void
   * @exception
   * @since 1.0.0
   */
  public void toObject(JSONObject json, T t) {
    if (json == null || t == null)
      return;
    toObject(json, t, t.getClass());
  }

  private void toObject(JSONObject json, T t, Class<?> c) {
    Class<?> s = c.getSuperclass();
    if (s != null) {
      toObject(json, t, s);
    }
    for (Field field : c.getDeclaredFields()) {
      try {
        field.setAccessible(true);
        Type type = field.getType();
        String fieldName = field.getName();
        if (String.class.equals(type) && json.has(fieldName))
          field.set(t, json.getString(fieldName));
      } catch (Exception e) {
        LoggerUtils.errorInfo("JSONToObject发生异常", e);
      }
    }
  }

  private void convert(JSONObject json, T t, Class<?> c) {
    Class<?> s = c.getSuperclass();
    if (s != null) {
      convert(json, t, s);
    }
    for (Field field : c.getDeclaredFields()) {
      try {
        Type type = field.getType();
        field.setAccessible(true);
        if (String.class.equals(type))
          json.put(field.getName(), field.get(t));
      } catch (Exception e) {
        LoggerUtils.errorInfo("ObjectToJSON发生异常", e);
      }
    }
  }
}
