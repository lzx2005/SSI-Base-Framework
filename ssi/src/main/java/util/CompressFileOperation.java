package util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * 压缩文件解压
 */
public class CompressFileOperation {
  /**
   * 初始化调用的方法
   * 
   * @param in 文件流
   * @param projectDir 项目路径
   * @param fileName 文件名
   * @return String 解压后的文件夹路径
   * @throws Exception
   */
  public static String getCompressFile(InputStream in, File f, String projectDir, String fileName) throws Exception {
    String fileDir = null;
    if (fileName.toLowerCase().endsWith(".rar")) {
    } else {
      fileDir = CompressFileOperation.unzip(in, projectDir, fileName);
    }
    return fileDir;
  }

  private static synchronized String unzip(InputStream in, String projectDir, String fileName) throws Exception {
    String destDir = CompressFileOperation.createDestDir(projectDir);
    CompressFileOperation.unzip(in, destDir);
    return destDir;
  }

  /**
   * 解压zip格式的压缩文件到指定目录下
   * 
   * @param InputStream zip文件流
   * @param destDir 解压目录
   * @throws Exception
   */
  private static void unzip(InputStream fis, String destDir) throws Exception {
    ZipInputStream zis = new ZipInputStream(fis);
    ZipEntry ze = null;
    FileOutputStream fos = null;
    BufferedOutputStream bos = null;
    try {
      while ((ze = zis.getNextEntry()) != null) {
        int size;
        byte[] buffer = new byte[1024];
        fos = new FileOutputStream(destDir + ze.getName());
        bos = new BufferedOutputStream(fos, buffer.length);

        while ((size = zis.read(buffer, 0, buffer.length)) != -1) {
          bos.write(buffer, 0, size);
        }
        bos.flush();
        bos.close();
        fos.close();
      }
    } catch (Exception e) {
      throw e;
    } finally {
      bos.flush();
      bos.close();
      fos.close();
      zis.closeEntry();
    }
  }

  /**
   * 根据传入文件路径,创建临时文件夹
   * 
   * @param directory 路径
   */
  private static String createDestDir(String projectDirectory) {
    String destDir = projectDirectory.toString() + "//temp//" + System.currentTimeMillis() + "//";

    // 根据不同的操作系统创建文件夹
    if (File.separator.equals("/")) {
      // 非windows系统
      destDir = destDir.replaceAll("\\\\", "/");
    } else {
      // windows系统
      destDir = destDir.replaceAll("/", "\\\\");
    }
    // 创建文件夹
    File dir = new File(destDir);
    if (!dir.exists() || !dir.isDirectory()) {
      dir.mkdirs();
    }
    return destDir;
  }

  /**
   * 删除指定文件
   * 
   * @param filePath 文件路径包括文件名
   */
  @SuppressWarnings("unused")
  private static void delFile(String filePath) {
    File file = new File(filePath);
    if (file.exists() && file.isFile())
      file.delete();
  }

  /**
   * 删除文件夹
   * 
   * @param folderPath 文件夹路径
   */
  public static void delFolder(String folderPath) {
    try {
      delAllFile(folderPath); // 删除文件夹下所有文件
      File filePath = new java.io.File(folderPath);
      filePath.delete(); // 删除空文件夹
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * 删除指定文件夹下所有文件
   * 
   * @param folderPath 文件夹路径
   */
  public static boolean delAllFile(String path) {
    boolean flag = false;
    File file = new File(path);
    if (!file.exists()) {
      return flag;
    }
    if (!file.isDirectory()) {
      return flag;
    }
    String[] tempList = file.list();
    File temp = null;
    for (int i = 0; i < tempList.length; i++) {
      if (path.endsWith(File.separator)) {
        temp = new File(path + tempList[i]);
      } else {
        temp = new File(path + File.separator + tempList[i]);
      }
      if (temp.isFile()) {
        temp.delete();
      }
      if (temp.isDirectory()) {
        delAllFile(path + "/" + tempList[i]);// 先删除文件夹里面的文件
        delFolder(path + "/" + tempList[i]);// 再删除空文件夹
        flag = true;
      }
    }
    return flag;
  }
}
