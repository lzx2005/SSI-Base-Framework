package util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日志工具类<br>
 * 本类提供了打印方法执行时间的性能的方法、用户行为跟踪日志的方法、外部系统调用webservice的日志方法、error日志方法、sso日志方法、
 * 方法运行时监控的日志方法、登录日志方法。<br>
 * 本类支持扩展新的日志文件，但要同时修改smweb工程下的logback.xml文件。
 * 
 * @author gebing
 * @author DingZhanhe
 * 
 */
public class LoggerUtils {

  // 性能日志
  private static Logger loggerPerformance = LoggerFactory.getLogger("performance");

  // 用户行为跟踪日志
  private static Logger loggerBehaviour = LoggerFactory.getLogger("behaviour");

  // 外部系统调用我们的webservice日志
  private static Logger loggerWebservice = LoggerFactory.getLogger("webservice");

  // error日志
  private static Logger loggerError = LoggerFactory.getLogger("error");

  // sso日志
  private static Logger loggerSso = LoggerFactory.getLogger("sso");

  // 监控方法执行的日志
  private static Logger loggerProfile = LoggerFactory.getLogger("profile");

  // 登录的日志
  private static Logger loggerLogin = LoggerFactory.getLogger("login");

  // 权限操作的日志
  private static Logger loggerRolePermission = LoggerFactory.getLogger("rolepermission");

  // 系统配置日志
  private static Logger loggerConfigfile = LoggerFactory.getLogger("sysconfig");

  // 调试用
  private static Logger loggerConsoleFile = LoggerFactory.getLogger("com.sohu");

  /**
   * 输出系统配置文件日志
   * 
   * @param msg
   */
  public static void debugConsole(String msg) {
    if (loggerConsoleFile.isDebugEnabled()) {
      loggerConsoleFile.debug(msg);
    }
  }

  /**
   * 输出系统配置文件日志
   * 
   * @param msg
   */
  public static void sysConfigDebug(String msg) {
    loggerConfigfile.debug(msg);
  }

  /**
   * 输出权限操作日志
   * 
   * @param msg
   */
  public static void rolePermissionInfo(String msg) {
    StringBuffer sb = getBaseInfo(msg);
    loggerRolePermission.info(sb.toString());
  }

  /**
   * 输出用户行为日志
   * 
   * @param msg
   */
  public static void behaviourInfo(String msg) {
    StringBuffer sb = getBaseInfo(msg);
    loggerBehaviour.info(sb.toString());
  }

  /**
   * 输出用户行为日志
   * 
   * @param msg
   */
  public static void behaviourWarn(String msg) {
    StringBuffer sb = getBaseInfo(msg);
    loggerBehaviour.warn(sb.toString());
  }

  /**
   * 输出性能分析日志
   * 
   * @exception
   * @since 1.0.0
   */
  public static void performanceDebug(String msg) {
    loggerPerformance.debug(msg);
  }

  /**
   * webservice调用日志
   * 
   * @exception
   * @since 1.0.0
   */
  public static void webserviceInfo(String msg) {
    loggerWebservice.info(msg);
  }

  /**
   * 输出error信息
   * 
   * @param msg
   */
  public static void errorInfo(String msg) {
    loggerError.error(msg);
  }

  /**
   * 输出error信息
   * 
   * @param msg
   * @param t
   */
  public static void errorInfo(String msg, Throwable t) {
    loggerError.error(msg, t);
  }

  /**
   * 输出sso登录信息
   * 
   * @param msg
   */
  public static void ssoInfo(String msg) {
    StringBuffer sb = getBaseInfo(msg);
    loggerSso.info(sb.toString());
  }

  /**
   * 记录登录信息
   * 
   * @param msg
   */
  public static void loginInfo(String msg) {
    StringBuffer sb = getBaseInfo(msg);
    loggerLogin.info(sb.toString());
  }

  /**
   * 输出方法执行的信息
   * 
   * @param msg
   */
  public static void profileDebug(String msg) {
    loggerProfile.debug(msg);
  }

  private static StringBuffer getBaseInfo(String msg) {
    StringBuffer sb = new StringBuffer();
    sb.append(msg);
    return sb;
  }

}
