package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 常用正则工具类
 * 
 * RegexpUtil
 * 
 * DingZhanhe 2013-3-29 下午5:26:25
 * 
 * @version 1.0.0
 * 
 */
public final class RegexpUtil {

  /**
   * 检测邮箱地址是否合法
   * 
   * @param email
   * @return boolean
   * @exception
   * @since 1.0.0
   */
  public static final boolean checkEmail(String email) {
    if (StringUtils.isBlank(email)) {
      return false;
    }
    String check = "^(\\w+[-|\\.]?)+\\w+@(\\w++(-\\w+)?\\.)+[a-zA-Z]{2,}$";
    Pattern regex = Pattern.compile(check);
    Matcher matcher = regex.matcher(email);
    boolean isMatched = matcher.matches();
    return isMatched;
  }
}
