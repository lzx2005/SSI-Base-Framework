package util;

import org.apache.commons.lang.ArrayUtils;

/**
 * 
 * <b>字符串工具类</b><br>
 * 
 * GeBing 2012-11-12 下午3:05:46
 * 
 * @version 1.0.0
 * 
 */
public class StringUtils extends org.apache.commons.lang.StringUtils {

  public static String firstToLowerCase(String str) {
    if (str != null && str.length() != 0) {
      return Character.toLowerCase(str.charAt(0)) + str.substring(1);
    }
    return str;
  }

  public static String firstToUpperCase(String str) {
    if (str != null && str.length() != 0) {
      return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }
    return str;
  }

  /**
   * 用指定的分隔符连接数组中的元素
   * 
   * @param array
   * @param separator
   * @return
   */
  public static String join(String[] array, String separator) {
    if (ArrayUtils.isEmpty(array))
      return "";
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < array.length; i++) {
      if (i > 0)
        builder.append(separator);
      builder.append(array[i]);
    }
    return builder.toString();
  }

  /**
   * 用指定字符串环绕源字符串。如wrap("hello", "'") = 'hello'
   * 
   * @param src
   * @param wrapper
   * @return
   */
  public static String wrap(String src, String wrapper) {
    return wrapper + src + wrapper;
  }

  /**
   * 用指定字符串环绕源字符串。如wrap("hello", "[", "]") = [hello]
   * 
   * @param src
   * @param head
   * @param tail
   * @return
   */
  public static String wrap(String src, String head, String tail) {
    return head + src + tail;
  }
}
