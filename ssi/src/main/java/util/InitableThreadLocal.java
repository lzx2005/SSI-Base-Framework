package util;

/**
 * 可以手动控制何时初始化的ThreadLocal，提升initialValue方法的访问级别到public
 * 
 * @author veggie.L @ 2013-4-12 上午11:13:15
 */
public abstract class InitableThreadLocal<T> extends ThreadLocal<T> {
  @Override
  public abstract T initialValue();

}
