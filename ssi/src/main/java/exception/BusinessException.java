package exception;

import java.text.MessageFormat;

/**
 * 业务异常
 * 
 * @author gebing
 * 
 */
public class BusinessException extends RuntimeException {

  private static final long serialVersionUID = 4606098461746957481L;

  public BusinessException(String s) {
    super(s);
  }

  public BusinessException(Exception t) {
    super(t);
  }

  public BusinessException(String pattern, Object... arguments) {
    super(MessageFormat.format(pattern, arguments));
  }

  public BusinessException(String s, Exception t) {
    super(s, t);
  }

  public BusinessException(Exception t, String pattern, Object... arguments) {
    super(MessageFormat.format(pattern, arguments), t);
  }
}
