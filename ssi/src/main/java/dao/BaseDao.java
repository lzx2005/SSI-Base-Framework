package dao;

import java.util.List;

import exception.BusinessException;


public interface BaseDao<T> {

  /**
   * 保存数据
   * 
   * @param sqlMapId
   *        SQLMapId
   * @param po
   *        Po对象
   * @return T Po对象
   * @throws BusinessException
   */
  public T save(String sqlMapId, T po) throws BusinessException;

  /**
   * 
   * 根据条件删除，返回删除数据条数
   * 
   * @params sqlMapId SQLMapId
   * @param params
   *        Po对象或Map对象
   * @return int 更新条数
   * @since 1.0.0
   */
  public int delete(String sqlMapId, Object params);

  /**
   * 
   * 只删除一条记录，否则抛出异常。
   * 
   * @param sqlMapId
   *        SQLMapId
   * @param params
   *        Po对象或Map对象
   * @return int 更新条数
   * @throws BusinessException
   * @since 1.0.0
   */
  public int deleteOne(String sqlMapId, Object params) throws BusinessException;

  /**
   * 更新数据
   * 
   * @param sqlMapId
   *        SQLMapId
   * @param params
   *        Po对象或Map对象
   * @return int 更新条数
   * @since 1.0.0
   */
  public int update(String sqlMapId, Object params);

  /**
   * 只更新一条记录。如果更新的非一条，则抛出异常。
   * 
   * @param sqlMapId
   *        SQLMapId
   * @param params
   *        Po对象或Map对象
   * @return int 更新条数
   * @throws BusinessException
   * @since 1.0.0
   */
  public int updateOne(String sqlMapId, Object params) throws BusinessException;

  /**
   * 
   * 根据ID取得数据
   * 
   * @param sqlMapId
   *        SQLMapId
   * @param id
   *        Po对象的主键
   * @return T Po对象
   * @since 1.0.0
   */
  public T queryById(String sqlMapId, Long id);

  /**
   * 查询数据集合
   * 
   * @param sqlMapId
   *        SQLMapId
   * @param params
   *        Po对象或Map对象
   * @return List<T> Po对象集合
   */
  public List<T> queryList(String sqlMapId, Object params);

  /**
   * 查询数据对象
   * 
   * @param sqlMapId
   *        SQLMapId
   * @param params
   *        Po对象或Map对象
   * @return T Po对象
   */
  public T queryObject(String sqlMapId, Object params);

  /**
   * 取得总条数
   * 
   * @param sqlMapId
   *        SQLMapId
   * @param params
   *        Po对象或Map对象
   * @return long 总条数
   */
  public long queryCount(String sqlMapId, Object params);

  /**
   * 
   * 根据Po类取序列值
   * 
   * @param clazz
   *        Po类
   * @return long 序列值
   * @since 1.0.0
   */
  public long getSeqValue(Class<?> clazz);

  /**
   * 
   * 根據SQLMapId得到序列值
   * 
   * @param sqlMapId
   * @return long 序列值
   * @since 1.0.0
   */
  public long getSeqValue(String sqlMapId);
}
