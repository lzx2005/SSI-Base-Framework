package constant;

/**
 * 系統常量类
 */
public interface Constant {
	
	String PROJECT_CODING = "UTF-8";
	
	String TARGET = "mainFrame";
	/** 字符串分隔符 , **/
	String COMMA_SEPARATOR = ",";
	/** 字符串分隔符 ; **/
	String COMMA_PATH_SEPARATOR = ";";
	/** 字符串分隔符 - **/
	String COMMA_DUHAO_LINE = "-";
	/** 下划线　'_' 分隔符 **/
	String UNDER_LINE = "_";
	/** &　连接符 **/
	String AND = "&";

	/** 成功标示 */
	String success = "1";
	/** 失败标示 */
	String failure = "0";
	/** 起始数 */
	String BEGIN_NUM = "BNUM";
	/** 结束数 */
	String END_NUM = "ENUM";
	/** 排序列 */
	String SORT_CELL = "SIDX";
	/** 排序方式 */
	String ID = "id";
	String SORT_WAY = "SORD";
	String CODE = "code";
	String TEXT = "text";
	String VALUE = "value";
	String UNIQUE = "unique";
	String NOTUNIQUE = "notUnique";
	String CLASSNAME = "className";

	public interface DataTypes {
		String TEXT = "TEXT";
		String DATE = "DATE";
		String NUMBER = "NUMBER";
		String MONEY = "MONEY";
		String INTEGER = "INTEGER";
		String YEAR = "YEAR";
	}

	/**
	 * 系统中1，0常量。
	 */
	public interface CommonManage {
		/** 数值常量 1 */
		Integer YES = 1;
		/** 数值常量 0 */
		Integer NO = 0;
	}

	/**
	 * 运行状态。
	 */
	public interface Status {
		/** 数值常量 0 */
		int run = 0;
		/** 数值常量 1 */
		int success = 1;
		/** 数值常量 2 */
		int error = 2;
	}

	/**
	 * 运行状态。
	 */
	public interface IsInterfaceRun {
		String Yes = "Y";
		String No = "N";
	}

	/**
	 * ajax请求结果
	 */
	public interface JsonResult {
		/**
		 * ajax请求成功
		 */
		String success = "success";
		/**
		 * ajax请求失败
		 */
		String failure = "failure";
		/**
		 * ajax请求成功-部分数据有问题
		 */
		String half = "half";
	}

	/**
	 * ajax请求JsonResultMessage的结果
	 */
	public interface JsonResultMessage {
		String success = "导入成功！";// 所有数据没有问题
		String error = "导入失败！";// 程序会有异常
		String moiety = "导入的数据有问题！";// 部分有问题，部分没有问题
	}
}
