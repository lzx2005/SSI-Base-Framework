package web.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.tapestry.json.JSONObject;
import org.springframework.util.CollectionUtils;

import service.BaseService;
import util.List2JSON;
import util.Page;
import util.Util;

/**
 * <b>分页基类 | Action</b><br>
 * 1.提供分页方法<br>
 * 2.提供获取分页JSON<br>
 * @version 1.0.0
 */
public class BasePageAction<T> extends BaseAction {

  private static final long serialVersionUID = 1L;
  protected static int DEFAULT_PAGE_SIZE = 10;// 每页默认数据显示条数

  // 当前页数
  protected Integer currentPageNo;
  // 每页的个数
  protected Integer currentPageSize;
  // 一共多少页
  protected Integer currentTotal;

  public void getPage(BaseService<T> service, Map<String, Object> params, String hostCellName, String[] showCellNames)
      throws Exception {
    // 增加状态过滤条件，页面上有此条件的就传过来，无此条件的则忽略
    HttpServletRequest req = ServletActionContext.getRequest();
    List<String> statusList = new ArrayList<String>();
    String statuses = req.getParameter("status");

    if (StringUtils.isNotBlank(statuses)) {
      statusList.addAll(Arrays.asList(statuses.split("\\,")));
    }
    if (!CollectionUtils.isEmpty(statusList)) {
      params.put("statusList", statusList);
    }

    printWriter(getPageData(service, params, hostCellName, showCellNames).toString());
  }

  public JSONObject getPageData(BaseService<T> service, Map<String, Object> params, String hostCellName,
      String[] showCellNames) throws Exception {
    HttpServletRequest request = ServletActionContext.getRequest();
    String page = request.getParameter("page");
    String rows = request.getParameter("rows");
    String sidx = request.getParameter("sidx");
    String sord = request.getParameter("sord");
    int pageNo = StringUtils.isNotEmpty(page) ? Integer.valueOf(page).intValue() : 1;
    int pageSize = StringUtils.isNotEmpty(rows) ? Integer.valueOf(rows).intValue() : DEFAULT_PAGE_SIZE;

    // 转换PagesMap
    convertPagesMap(params, new Page<T>(), pageNo, pageSize, sidx, sord);
    // 查询总条数
    long count = service.queryPageCount(params);
    // 查询当前页数据
    List<T> objs = service.queryPage(params);
    // 获取总页数
    int total = getTotal(count, pageSize);
    if (pageNo > total) {
      pageNo = total;
    }
    return new List2JSON<T>().toJSON(pageNo, count, total, objs, hostCellName, showCellNames);
  }

  @SuppressWarnings("rawtypes")
  public Map<String, Object> getPageParams() {
    HttpServletRequest request = ServletActionContext.getRequest();
    
    currentPageNo = StringUtils.isNotEmpty(request.getParameter("page")) ? Integer.valueOf(request.getParameter("page")).intValue() : 1;
    currentPageSize = StringUtils.isNotEmpty(request.getParameter("rows")) ? Integer.valueOf(request.getParameter("rows")).intValue() : DEFAULT_PAGE_SIZE;

    String sidx = request.getParameter("sidx");
    String sord = request.getParameter("sord");
    Map<String, Object> params = new HashMap<String, Object>();
    // 转换PagesMap
    convertPagesMap(params, new Page(), currentPageNo, currentPageSize, sidx, sord);
    return params;
  }

  public void setCurrentPageNumber(Long count) {
    currentTotal = getTotal(count, currentPageSize);
    if (currentPageNo > currentTotal) {
      currentPageNo = currentTotal;
    }
  }

  /**
   * 
   * 转换PagesMap
   * 
   * @param pages
   * @param pageNo
   *        当前页
   * @param pageSize
   *        每页显示条数
   * @param sidx
   *        排序字段
   * @param sord
   *        排序方式
   * @return Map<String,Object>
   * @exception
   * @since 1.0.0
   */
  protected void convertPagesMap(Map<String, Object> params, Page<?> pages, int pageNo, int pageSize, String sidx,
      String sord) {
    pages.setPageNo(pageNo);
    pages.setPageSize(pageSize);
    if (StringUtils.isNotEmpty(sidx)) {
      pages.setOrderBy(sidx);
      pages.setOrder(sord);
    }
    Util.getPageParames(pages, params);
  }

  /**
   * 取得总页数
   * 
   * @param count
   *        总条数
   * @param pageSize
   *        每页显示条数
   * @return int
   * @exception
   * @since 1.0.0
   */
  protected int getTotal(long count, int pageSize) {
    return count > 0 ? (int) ((count + pageSize - 1) / pageSize) : 0;
  }

  public Integer getCurrentPageNo() {
    return currentPageNo;
  }

  public void setCurrentPageNo(Integer currentPageNo) {
    this.currentPageNo = currentPageNo;
  }

  public Integer getCurrentPageSize() {
    return currentPageSize;
  }

  public void setCurrentPageSize(Integer currentPageSize) {
    this.currentPageSize = currentPageSize;
  }

  public Integer getCurrentTotal() {
    return currentTotal;
  }

  public void setCurrentTotal(Integer currentTotal) {
    this.currentTotal = currentTotal;
  }

}
