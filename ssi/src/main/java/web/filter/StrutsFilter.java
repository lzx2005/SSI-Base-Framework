package web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.dispatcher.ng.filter.StrutsPrepareAndExecuteFilter;

/**
 * webapp/static目錄下的資源可直接訪問，無需經過StrutsPrepareAndExecuteFilter
 * 
 * StrutsFilter
 * @version 1.0.0
 * 
 */
public class StrutsFilter extends StrutsPrepareAndExecuteFilter {

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    super.init(filterConfig);

    try {
      // 屏蔽struts标签解析时后台输出的大量垃圾日志!
      freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest) req;
    String uri = request.getRequestURI();
   /* if ("".equals(uri) || "/".equals(uri) || uri.endsWith("/index.jsp") || uri.endsWith("/403.jsp")
        || uri.endsWith("/favicon.ico")) {
      chain.doFilter(req, res);
      return;
    }*/

 /*   if (request.getSession().getAttribute("userName") == null) {
      if (request.getParameter("userName") == null) {
        ((HttpServletResponse) res).sendRedirect("/403.jsp");
        return;
      } else {
        request.getSession().setAttribute("userName", request.getParameter("userName"));
      }
    }*/
    super.doFilter(req, res, chain);
  }

  @Override
  public void destroy() {
    super.destroy();
  }

}
