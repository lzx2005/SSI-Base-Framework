<%@ page language="java" import="java.util.Date" pageEncoding="utf-8"%>
<% request.setAttribute("tm", new Date().getTime()); %>
<meta http-equiv="Cache-Control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<link rel="stylesheet" type="text/css" media="screen" href="${ctx}/css/themes/redmond/jquery-ui-1.8.2.custom.css" />
<link rel="stylesheet" type="text/css" media="screen" href="${ctx}/css/themes/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" href="${ctx}/css/css.css?t=${tm}" />
<script type="text/javascript" src="${ctx}/js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="${ctx}/js/json.js"></script>
<script type="text/javascript" src="${ctx}/js/plugins/jqGrid/js/jquery.jqGrid.src.js"></script>
<script type="text/javascript" src="${ctx}/js/plugins/jqGrid/src/grid.custom.js"></script>
<script type="text/javascript" src="${ctx}/js/plugins/jqGrid/js/i18n/grid.locale-cn.js"></script>

<script type="text/javascript" src="${ctx}/js/jGrid-util.js"></script>
<script type="text/javascript" src="${ctx}/js/plugins/DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="${ctx}/js/core.js?t=${tm}"></script>
<script type="text/javascript" src="${ctx}/js/jquery.form.js"></script>

<script type="text/javascript" src="${ctx}/js/jquery-ui-1.8.2.custom.min.js"></script>

<script type="text/javascript" src="${ctx}/js/plugins/numericInput/numericInput.js"></script>

<script type="text/javascript" src="${ctx}/js/ajaxfileupload.js" ></script>
<script type="text/javascript">

	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
	var ctx = "${ctx}";
	$.jgrid.ajaxOptions.type = 'post';
</script>