<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>BI系统-excel导入</title>
<%@ include file="/common/css-js.jsp"%> 
<%@ include file="/common/css-js-multiselect.jsp"%>
	<script>
//从Excel导入
function importExcel(){
	var yearName = $("#bg_year").val();
	var sence = $("#bg_sence").val();
	var version = $("#bg_version").val();
	var canWrite = $("#canWrite").val();
	if (canWrite != "1") {
		/* var state="不可录入";
		if(canWrite=="0"){
			state="已锁定";
		}else{
			state="未开启";
		} */
		alert("【" + yearName + "】年--【" + sence + "】场景--【" + version + "】版本 状态为不可录入，请切换版本!");
		return false;
	}
	$("#importForm").resetForm();
	$("#upload-confirm-div").dialog({
		position: 'center',
		modal: true,
		resizable: false,
		title: '选择上载文件',
		width: 340,
		height: 160,
		draggable: false,
		buttons: {
			'取消': function() {
				$(this).dialog('destroy');
			},
			'导入': function() {
				var myexcel=$("#myexcel").val();
				if(myexcel.length==0){
					alert("文件不能为空,请选择excel文件上传！");
					return false;
				}
				var end=myexcel.substring(myexcel.lastIndexOf("\.")+1,myexcel.length);
				if(!(end=="xlsx"||end=="xls")){
					alert("只能上传后缀名为.xls或.xlsx的文件!");
					return false;
				}
	
				if(window.confirm("执行导入后，将会替换系统内重复的数据，请问是否继续？")){
					$(this).dialog('destroy');
					showMaskLayer();
					// 上传文件
					$.ajaxFileUpload({
							url: '${ctx}/pr/importExcel?params='+$("#xForm").serialize(),
							data: {
								'yearName' : $("#bg_year").val(),
								'sence' : $("#bg_sence").val(),
								'version' : $("#bg_version").val(),
								'model':"<s:property value='#request.model'/>",
								'viewName':"<s:property value='#request.viewName'/>",
								'fileName':$("#myexcel").val().substring($("#myexcel").val().lastIndexOf("\\") + 1),
								'myexcel' : myexcel
							},
							secureuri: false,
							fileElementId: 'myexcel',
							dataType: 'json',
							success: function(data, status){
						 		//var type=data.type;
								showValidateInfoDiv(data, false);
							}
						});
			}}
		}
	});
}

//弹出验证信息层
function showValidateInfoDiv(data, isEdit){
	closeMaskLayer();
	if(data && data.success){
		var viewName="<s:property value='#request.viewName'/>";
		if(isEdit){
			alert("保存成功!");
			if(viewName=="y"){
				changeProjectForOption();
			}
		} else {
			alert("数据导入成功，本次共成功导入数据" + data.dataCount + "条!");
			/* var url = ctx+'/pr/load?model='+'<s:property value='#request.model'/>'+'&viewName=z&templateCode='+'<s:property value='#request.model'/>'+'_PR_INPUT';
			window.location.href = url; */
			$("#queryBtn").trigger("click");
			if(viewName=="y"){
				changeProjectForOption();
			}
			//自动提示 项目1 2 值集
			changeProjectForAuto();
			project1Arr=[];
	    	project2Arr=[];
			$('#edit-validate-div').dialog('destroy'); 
		}
	} else {
		if(isEdit){
			$("#a_reimport").hide();
			$("#span_import").hide();
			// 如果数据验证失败，将保存前处于编辑状态的行置回编辑状态.
		} else {
			$("#a_reimport").show();
			$("#span_import").show();
			$("#span_import_filename").html(data.fileName);
		}
		$("#a_validate_info_export").attr("href", "${ctx}/commontemplate/exportvi?uuid=" + data.uuid);
		var nhtml = "";
		for(var i = 0; i < data.messages.length; i++){
			var vi = data.messages[i];
			nhtml += "<tr><td align='center' valign='top'>" + vi['line'] + "</td><td>" + vi['message'] + "</td></tr>";
		}
		$("#edit-validate-tbody").html(nhtml);
		$("#edit-validate-div").dialog({
			position: 'center',
			modal: true,
			resizable: false,
			title: '检查结果',
			width: 600,
			height: 360,
			draggable: false
		});
	}
}
</script>
</head>

<body>
<div style="display: none;">
	<div id="edit-validate-div">
		<div class="JgopDes">
			<div id="div_operation" class="mRsarOp2">
				<a id="a_reimport" href="javascript:importExcel();" class="geyBtn" style="display: none;">重新导入</a>
				<a id="a_validate_info_export" href="javascript:void(0);" class="geyBtn">检查结果导出</a>
				<a href="javascript:void(0);" onclick="$('#edit-validate-div').dialog('destroy');" class="geyBtn2">返 回</a>
			</div>
		</div>
		<span id="span_import" style="display: none;">当前导入文件：<span id="span_import_filename"></span></span>
		<div><table border="1" width="100%">
			<thead><tr><td width="20%" valign="top" align="center">出错行号</td><td width="80%" align="center">原因</td></tr></thead>
			<tbody id="edit-validate-tbody">
			</tbody>
		</table></div>
	</div>

	<div id="upload-confirm-div">
		<!-- 防止复制粘贴剪切右键的文本框. -->
		<input type="text" onpaste="return false" oncopy="return false" oncut="return false" oncontextmenu="return false" style="display: none;">
		<form id="importForm" name="importForm" action="${ctx}/pr/importExcel" method="post" enctype="multipart/form-data">
			<input type="file" id="myexcel"name="myexcel" cssStyle="height: 28px; font-size: 16px;" accept="xls application/vnd.ms-excel,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
		</form>
	</div>
</div>
</body>