<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><s:property value="templateName" />_PR新增数据</title>
<%@ include file="/common/css-js.jsp"%>
<script type="text/javascript" src="${ctx}/js/pr.js"></script>
<%@ include file="/common/css-js-multiselect.jsp"%>
<script type="text/javascript">
//取得编辑选项
function getEditOptions(){
	return {value:"${selectOptions}","class":"jqgrid-row-editing-select jgedit-select"};
}
 	$(function() {
		initPageBgMapping();//设置原场景、版本、年
		var result = "<s:property value='#request.canSync'/>";
		//alert(result);
		var syncDate = "<s:property value='#request.syncDate'/>";
		var model = "<s:property value='#request.model'/>";
		var syncPeople = "<s:property value='#request.syncPeople'/>";
		
		if (syncDate.length != 0) {
			$("#syncDate").html(syncDate);
		}
		if (syncPeople.length != 0) {
			$("#syncPeople").html(syncPeople);
		}
		//switchBgMapping(true,model,"x",ctx);
		if (model == "FOCUS") {
			$("#bizlineTd").hide();
			$("#bizlineTd2").hide();
		}
		changeType(model);
		//版本是未开启状态时，不显示修改人、修改时间
		hiddenUpdateUserAndTime();
		
		if (result == "true") {
			gridFun();
			normalBtn();
			optionNone();//根据场景版本状态，锁定按钮
			//自动提示 项目1 2 值集
			changeProjectForAuto();
		} else {
			disableBtn();
			sycnMsgDialog($("#syncPeople").html(),'init');
			//alert("正在同步中,同步，提交数据，保存等操作不可用！");
		}
		resizeGrid("userTable");
		
		
	});

	//js method control
	function check(param) {
		$.post("${ctx}/pr/checkButtonForFullName?t=" + new Date().getTime()+"&model="+'<s:property value='#request.model'/>', function(data, state) {
			// alert(data);
			if (data.result == "ok") {
				//判断场景版本是否切换  
				if(isChangeVersion()){
					if(window.confirm("您已改变了场景版本,是否切换?")){
						projcectValue_select();//保存 project值
						$("#switchBgMapping").trigger("click");
						return;
					}
					else{//修改场景版本信息  稍候做
						backVersion();
					}
					
				}
				//查询、导出操作  判断是否之前存在删除记录、未保存
				if (param == "query"||param == "exportExcel"||param == "importExcel"||param == "syncDate") {
					if(isExitSaveOption()&&!window.confirm("当前界面模版中有为保存的数据,是否真的要离开界面?")){
						return;
					}
				}
				if (param == "saveBtn") {
					saveBtn();
				} else if (param == "query") {
					query();
				} else if (param == "importExcel") {
					importExcel();
				} else if (param == "exportExcel") {
					exportExcel();
				} else if (param == "syncDate") {
					syncDate();
				}
			} else {
				disableBtn();
				sycnMsgDialog(data.req_by,'');
				//alert("正在执行同步请等候。。。");
			}
		});
	}
	/**
	 * 查询
	 */
	function query() {
		jQuery("#userTable").setGridParam({postData:null});//清除之前表单postDate数据，防止重复查询
		
		setTypeForQuery();//保存 业务线 费用大小类值 查询使用
		var params = $("#xForm").serializeObject();
		
		jQuery("#userTable").setGridParam({
			page:1,
			url : "${ctx}/pr/query?viewName=x&model=<s:property value='#request.model'/>",
			postData:params//发送数据
		}).trigger("reloadGrid");
	}
	
	/**
	 * 数据同步
	 */
	function syncDate() {
		if (!confirm("是否确认同步操作？")) {
			return;
		}
		//1设置本页面连接不可用-样式变化
		disableBtn();
		//调用后台sync函数
		$.post("${ctx}/pr/syncData?step=step1&viewName=x&model=<s:property value='#request.model'/>", function(data, state) {
			alert(data);
			if (state == "success") {
				$.post("${ctx}/pr/syncData?step=step2&viewName=x&model=<s:property value='#request.model'/>", function(data, state) {
					gridFun();
					normalBtn();
					//自动提示 项目1 2 值集
					changeProjectForAuto();
					project1Arr=[];
			    	project2Arr=[];
					alert(data);
				});
			}
		});
	}

	/**
	 * grid保存
	 */
	function saveBtn() {
		var model = "<s:property value='#request.model'/>";
		var yearName = $("#bg_year").val();
		var sence = $("#bg_sence").val();
		var version = $("#bg_version").val();
		var selectedIds = jQuery("#userTable").jqGrid('getGridParam', 'selarrrow');
		if (selectedIds.length == 0) {
			alert("请至少选择1条数据!");
			return;
		}
		var canWrite = $("#canWrite").val();
		if (canWrite != "1") {
			/* var state="不可录入";
			if(canWrite=="0"){
				state="已锁定";
			}else{
				state="未开启";
			} */
			alert("【" + yearName + "】年--【" + sence + "】场景--【" + version + "】版本 状态为不可录入，请切换版本!");
			return;
		}
		//编辑保存列前，执行一遍假保存.make sure cell is ok!
		var dataIds = jQuery('#userTable').jqGrid('getDataIDs');
		for ( var i = 0; i < dataIds.length; i++) {
			jQuery('#userTable').jqGrid('saveRow', dataIds[i]);
		}
		if('<s:property value='#request.model'/>'=='FOCUS'){
		
			for ( var i = 0; i < selectedIds.length; i++) {
				if(selectedIds[i]==201301||selectedIds[i]==201302){
					continue;
				}
				var ret = jQuery("#userTable").jqGrid('getRowData', selectedIds[i]);
				//alert(ret.project1);
				if(ret.project1.length==0){
					alert("项目1不能为空，请重新填写");
					return;
				}
			}
		}
		
		/* if (!confirm("是否确认添加到【" + yearName + "】年--【" + sence + "】场景--【" + version + "】版本么?")) {
			return;
		} */
		if(!window.confirm("数据将被保存在["+yearName+"年]["+sence+"场景]["+version+"版本]下,确认保存?")){
			return;
		}
		var jsonArr = "[";
		for ( var i = 0; i < selectedIds.length; i++) {
			//排除 对本页合计行 与 查询结果合计行  操作
			if(selectedIds[i]==201301||selectedIds[i]==201302){
				continue;
			} 
			var ret = jQuery("#userTable").jqGrid('getRowData', selectedIds[i]);
			
			jQuery('#userTable').jqGrid('saveRow', selectedIds[i]);
			var project1 = ret.project1;
			/* if(page_model=='FOCUS'&&ret.project1=='请选择'){
				project1 = '';
			} */
			//jsonArr+=JSON.stringify(ret)+","; or jsonArr+=JsonToStr(ret);
			jsonArr += "{model:\"" + model + "\",purchaseNumber:\"" + ret.purchase_number + "\",bizline:\"" + ret.bizline + "\",expenseType:\"" + ret.expenseType + "\",expenseSubtype:\""
					+ ret.expenseSubtype + "\",project1:\"" + project1 + "\",project2:\"" + ret.project2 + "\",yearName:\"" + yearName + "\",sence:\"" + sence + "\",version:\"" + version + "\",remark:\"" + ret.remark + "\"},";
		}
		jsonArr = jsonArr.substring(0, jsonArr.lastIndexOf(",")) + "]";
				showMaskLayer();
		$.post("${ctx}/pr/editSave?viewName=x&params="+$("#xForm").serialize(), "jsonArrStr=" + encodeURIComponent(jsonArr), function(data, state) {
			if (data == "OK") {
				query();
				closeMaskLayer();
				//自动提示 项目1 2 值集
				changeProjectForAuto();
				project1Arr=[];
		    	project2Arr=[];
				alert("保存成功!");
			} else {
				closeMaskLayer();
				alert("保存失败!");
			}
		});
	}
	/**
	 * grid加载
	 */
	function gridFun() {
		setTypeForQuery();//保存 业务线 费用大小类值 查询使用
		jQuery("#userTable").jqGrid({
			url : "${ctx}/pr/query?viewName=x&model=<s:property value='#request.model'/>&t=" + new Date().getTime(),
			datatype : "json",
			mtype : "POST",
			postData:$("#xForm").serializeObject(),
			colNames : ['index','<span style=\'color:red;\'>审核日期</span>','<span style=\'color:red;\'>PR单号</span>','PR单说明',<s:if test="#request.model=='FOCUS'">'<span style=\'color:red;\'>项目1</span>'</s:if><s:else>'项目1'</s:else>,'项目2','供应商','部门','<span style=\'color:red;\'>业务线</span>','<span style=\'color:red;\'>费用大类</span>',<s:if test="#request.model=='SOHU'||#request.model=='FOCUS'">'<span style=\'color:red;\'>费用小类</span>'</s:if><s:else>'费用小类'</s:else>,'<span style=\'color:red;\'>申请金额</span>','服务范围From','服务范围To','更新人','财务备注','上一年度10月','上一年度11月','上一年度12月','以前年度合计','1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月','本年合计','下一年度1月','下一年度2月','下一年度3月','下一年度4月','下一年度5月','下一年度6月','下一年度7月','下一年度8月','下一年度9月','下一年度10月','下一年度11月','下一年度12月','下一年度合计','备注'],
			colModel : [ {name : 'index', index : 'index',hidden:true,frozen:true},
			             {name : 'approvalDate', index : 'approval_date',frozen:true},
			             {name : 'purchase_number', index : 'purchase_number'},
			             {name : 'purchaseName', index : 'purchase_name'},
			             {name : 'project1', index : 'project1',<s:if test="#request.model=='AUTO'">editable:false</s:if><s:else>editable:true</s:else><s:if test="#request.model=='FOCUS'">,edittype:"select",editoptions:getEditOptions()</s:if>},//焦点是options
			             {name : 'project2', index : 'project2',<s:if test="#request.model=='AUTO'">editable:false</s:if><s:else>editable:true</s:else>},
			             {name : 'vendorName', index : 'vendor_name'},
			             {name : 'dept', index : 'dept'},
			             {name : 'bizline', index : 'bizline'},
			             {name : 'expenseType', index : 'expense_type'},//expenseType
			             {name : 'expenseSubtype', index : 'expense_subtype'},//expenseSubtype
			             {name : 'purchaseAmount', index : 'purchase_amount',align : 'right',formatter: 'currency'},
			             {name : 'serviceBeginDate', index : 'service_begin_date',formatter:'date', formatoptions:{newformat:'Y-m-d'}},
			             {name : 'serviceEndDate', index : 'service_end_date',formatter:'date', formatoptions:{newformat:'Y-m-d'}},
			             {name : 'userName', index : 'user_name'},
			             {name : 'approverDesc', index : 'bizline'},
			             {name : 'lastOct', index : 'LAST_OCT',align : 'right',formatter: 'currency'},
			             {name : 'lastNov', index : 'LAST_NOV',align : 'right',formatter: 'currency'},
			             {name : 'lastDec', index : 'LAST_DEC',align : 'right',formatter: 'currency'},
			             {name : 'lastYear', index : 'LAST_YEAR',align : 'right',formatter: 'currency'},
			             {name : 'jan', index : 'JAN',width :80,align : 'right',formatter: 'currency'},
			             {name : 'feb', index : 'FEB',align : 'right',formatter: 'currency'},
			             {name : 'mar', index : 'MAR',align : 'right',formatter: 'currency'},
			             {name : 'apr', index : 'apr',align : 'right',formatter: 'currency'},
			             {name : 'may', index : 'may',align : 'right',formatter: 'currency'},
			             {name : 'jun', index : 'jun',align : 'right',formatter: 'currency'},
			             {name : 'jul', index : 'jul',align : 'right',formatter: 'currency'},
			             {name : 'aug', index : 'aug',align : 'right',formatter: 'currency'},
			             {name : 'sep', index : 'sep',align : 'right',formatter: 'currency'},
			             {name : 'oct', index : 'OCT',align : 'right',formatter: 'currency'},
			             {name : 'nov', index : 'NOV',align : 'right',formatter: 'currency'},
			             {name : 'dec', index : 'dec',align : 'right',formatter: 'currency'},
			             {name : 'year', index : 'year',align : 'right',formatter: 'currency'},
			             {name : 'nextJan', index : 'NEXT_JAN',align : 'right',formatter: 'currency'},
			             {name : 'nextFeb', index : 'NEXT_FEB',align : 'right',formatter: 'currency'},
			             {name : 'nextMar', index : 'next_Mar',align : 'right',formatter: 'currency'},
			             {name : 'nextApr', index : 'next_Apr',align : 'right',formatter: 'currency'},
			             {name : 'nextMay', index : 'next_may',align : 'right',formatter: 'currency'},
			             {name : 'nextJun', index : 'next_jun',align : 'right',formatter: 'currency'},
			             {name : 'nextJul', index : 'next_jul',align : 'right',formatter: 'currency'},
			             {name : 'nextAug', index : 'next_aug',align : 'right',formatter: 'currency'},
			             {name : 'nextSep', index : 'next_sep',align : 'right',formatter: 'currency'},
			             {name : 'nextOct', index : 'NEXT_OCT',align : 'right',formatter: 'currency'},
			             {name : 'nextNov', index : 'NEXT_NOV',align : 'right',formatter: 'currency'},
			             {name : 'nextDec', index : 'NEXT_DEC',align : 'right',formatter: 'currency'},
			             {name : 'nextYear', index : 'next_Year',align : 'right',formatter: 'currency'},
			             {name : 'remark', index : 'remark',editable:true,sortable:false}
			           ],
			rowNum : 10,
			rowList : [ 10, 15, 20 ], //可调整每页显示的记录数 
			pager : '#jGpage',
			rownumbers : true,
			//cellEdit:true,//单元格可编辑
			multiselect : true,//多选
			cellurl : "clientArray",//不会向server端触发请求
			editurl : "clientArray",//不会向server端触发请求
			sortname : 'approval_date',
			sortable:false,
			sortorder : "desc",
			autowidth : true,
			autoScroll: false,
			height : '428',
			fixed:false,
			shrinkToFit : false,
			loadComplete : function(data) {
				var model = "<s:property value='#request.model'/>";
				if (model == "FOCUS") {
					$("#userTable").jqGrid('hideCol', "bizline");
				}
				$("#count").html($("#userTable").jqGrid('getGridParam','records'));//获取当前jqGrid的总记录数；
				generateSumDataForPr(data);
				noResultMessage("userTable");
			},
			onCellSelect : function(rowid, index, contents, event) {//当点击单元格时触发行选中
				if(rowid==201301||rowid==201302) return;//判断合计行
				$('#userTable').jqGrid('setSelection', rowid, false);
				//汽车 项目1 项目2 只读
				if((page_model!= "AUTO" &&(index==7||index==6||index==48))||(page_model== "AUTO" &&index==48)){
					/* if(page_model== "FOCUS" &&contents=='&nbsp;'&&index==6){
						$('#userTable').jqGrid('setSelection', rowid, false);
					} */
					$('#userTable').jqGrid('editRow', rowid, false);
					var project_1 = ''+rowid+'_project1';
					var project_2 = ''+rowid+'_project2';
					var remark_1 = ''+rowid+'_remark';
					$('#'+project_1).change(function(){
				        //alert($(this).val());  //弹出select的值
				        var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_'));
				        if(!checkRowStatus(row_id)){
				        	$('#userTable').jqGrid('setSelection', row_id, false);
				        }
				    });
					$('#'+project_2).change(function(){
				        //alert($(this).val());  //弹出select的值
						var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_'));
				        if(!checkRowStatus(row_id)){
				        	$('#userTable').jqGrid('setSelection', row_id, false);
				        }
				    });
					$('#'+remark_1).change(function(){
				        //alert($(this).val());  //弹出select的值
						var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_'));
				        if(!checkRowStatus(row_id)){
				        	$('#userTable').jqGrid('setSelection', row_id, false);
				        }
				    });
					var model = "<s:property value='#request.model'/>";
					if(index==7||index==6){
						//自动提示初始化
						initAutoCompletion();
					    //自动提示
						autoCompletion($("#"+rowid),"x");
					}
				}
				else if(index==1){
					$('#userTable').jqGrid('setSelection', rowid, false);
				}
			}/* , 
			onSelectRow : function(rowid) {
				
			},
			onSelectAll:function(rowids,status){
			} */
		});
		
		jQuery("#userTable").jqGrid('setFrozenColumns');
		
	}
	
	// 生成合计内容
	function generateSumDataForPr(data){
		if(data.rows.length == 0)
			return;
		var data_1 ={'index':"",
			        'approvalDate':"<b>本页合计</b>",
			        'purchase_number':"",
			        'purchaseName':"",
			        'project1':"",
			        'project2':"",
			        'vendorName':"",
			        'dept':"",
			        'bizline':"",
			        'expenseType':"",
			        'expenseSubtype':"",
			        'purchaseAmount':data.page_pur,
			        'serviceBeginDate':"",
			        'serviceEndDate':"",
			        'userName':"",
			        'approverDesc':"",
			        'lastOct':data.page_last10,
			        'lastNov':data.page_last11,
			        'lastDec':data.page_last12,
			        'lastYear':data.page_lastTotal,
			        'jan':data.page_this01,
			        'feb':data.page_this02,
			        'mar':data.page_this03,
			        'apr':data.page_this04,
			        'may':data.page_this05,
			        'jun':data.page_this06,
			        'jul':data.page_this07,
			        'aug':data.page_this08,
			        'sep':data.page_this09,
			        'oct':data.page_this10,
			        'nov':data.page_this11,
			        'dec':data.page_this12,
			        'year':data.page_thisTotal,
			        'nextJan':data.page_next01,
			        'nextFeb':data.page_next02,
			        'nextMar':data.page_next03,
			        'nextApr':data.page_next04,
			        'nextMay':data.page_next05,
			        'nextJun':data.page_next06,
			        'nextJul':data.page_next07,
			        'nextAug':data.page_next08,
			        'nextSep':data.page_next09,
			        'nextOct':data.page_next10,
			        'nextNov':data.page_next11,
			        'nextDec':data.page_next12,
			        'nextYear':data.page_nextTotal,
			        'remark':""}; 
		
		var data_2 ={'index':"",
			        'approvalDate':"<b>查询结果合计</b>",
			        'purchase_number':"",
			        'purchaseName':"",
			        'project1':"",
			        'project2':"",
			        'vendorName':"",
			        'dept':"",
			        'bizline':"",
			        'expenseType':"",
			        'expenseSubtype':"",
			        'purchaseAmount':data.total_pur,
			        'serviceBeginDate':"",
			        'serviceEndDate':"",
			        'userName':"",
			        'approverDesc':"",
			        'lastOct':data.total_last10,
			        'lastNov':data.total_last11,
			        'lastDec':data.total_last12,
			        'lastYear':data.total_lastTotal,
			        'jan':data.total_this01,
			        'feb':data.total_this02,
			        'mar':data.total_this03,
			        'apr':data.total_this04,
			        'may':data.total_this05,
			        'jun':data.total_this06,
			        'jul':data.total_this07,
			        'aug':data.total_this08,
			        'sep':data.total_this09,
			        'oct':data.total_this10,
			        'nov':data.total_this11,
			        'dec':data.total_this12,
			        'year':data.total_thisTotal,
			        'nextJan':data.total_next01,
			        'nextFeb':data.total_next02,
			        'nextMar':data.total_next03,
			        'nextApr':data.total_next04,
			        'nextMay':data.total_next05,
			        'nextJun':data.total_next06,
			        'nextJul':data.total_next07,
			        'nextAug':data.total_next08,
			        'nextSep':data.total_next09,
			        'nextOct':data.total_next10,
			        'nextNov':data.total_next11,
			        'nextDec':data.total_next12,
			        'nextYear':data.total_nextTotal,
			        'remark':""}; 
        jQuery('#userTable').jqGrid('addRowData', 201301, data_1, '');
		jQuery('#userTable').jqGrid('addRowData', 201302, data_2, '');
		
		$("tr[id=201301]>td:lt(2)", $("#userTable")).html("");
		$("tr[id=201301]>td:lt(2)", $("#userTable_frozen")).html("");
		
		$("tr[id=201302]>td:lt(2)", $("#userTable")).html("");
		$("tr[id=201302]>td:lt(2)", $("#userTable_frozen")).html("");
	}
</script>
</head>
<body>
<div style="display:none">
	<s:select id="project1_Auto" list="project1Map"  headerKey="" headerValue="请选择"/>
	<s:select id="project2_Auto" list="project2Map"  headerKey="" headerValue="请选择"/>
</div>
<div class="mainCont">
	<div class="mainRight">
		<div id="div_title" class="mTitleBig">
			<h3>
				<span><s:property value="templateName" />_PR新增数据</span>
				<s:if test="template.temTip != null && template.temTip != ''"><a title="查看操作提示" href="${ctx}/commontemplate/showhelp?templateCode=${template.temCode}" target="_blank"><span class="tipSpan"></span></a>
				</s:if>
			</h3>
			<div class="clear">&nbsp;</div>
		</div>
<form id="xForm" method="post" action="${ctx}/pr/export?viewName=x&model=<s:property value='#request.model'/>">
	<input  id="bg_viewName"  value="x" type="hidden" />
	<input  id="bg_model" name="prListBuluQuery.model" value="<s:property value='#request.model'/>" type="hidden" />
		<div id="div_bg" class="mRcont">
			<div class="mTitle mTitlebot">
				<h4>年度、场景、版本选择</h4>
				<div class="mRsearchGen">
					<span>年：</span>
					<s:select id="bg_year" list="{2013,2014}" name="prListBuluQuery.yearName"></s:select>
					<span>场景：</span>
					<s:select id="bg_sence" list="{'Q1Fcst', 'Q1Latest', 'Q2Fcst', 'Q2Latest', 'Q3Fcst', 'Q3Latest', 'Q4Fcst', 'Q4Latest', 'AnnualBudget', 'Actual'}" name="prListBuluQuery.sence"></s:select>
					<span>版本：</span>
					<s:select id="bg_version" list="{'V1', 'V2', 'V3', 'Final', 'Test'}" name="prListBuluQuery.version"></s:select>
					<span>状态：</span>
					<span class="mRMap" id="span_bg_allow">${prListBuluQuery.allow == 0 ? '已锁定' : (prListBuluQuery.allow == 1 ? '可录入' : '未开启')}</span>
					<span id="span_bg_allow_pic" class="${prListBuluQuery.allow == 0 ? 'allow0' : (prListBuluQuery.allow == 1 ? 'allow1' : 'allow2')}"></span>
					<input type="hidden" id="canWrite" value="${prListBuluQuery.allow}" />
					<a id="switchBgMapping" class="subUse subBtn" href="javascript:void(0);" onclick="switchBgMapping(false,'<s:property value="#request.model"/>','x',ctx);">切换</a>
				</div>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
			<div id="div_search">
				<div class="mTitle" style="background: none;">
					<h4>
						查询条件选择&nbsp;
						<a href="javascript:toggleSearchArea();">显示/折叠查询区域</a>
					</h4>
					<div class="clear">&nbsp;</div>
				</div>
				<div class="mRsearch" id="div_search_cond" style="display: none">
					<table>
						<tr>
							<td class="mRsTitle">审核日期：</td>
							<td>
								<input name="prListBuluQuery.approvalBegindate" type="text" class="Wdate" onclick="WdatePicker();" readonly="readonly" />
								<var>-</var>
								<input name="prListBuluQuery.approvalEnddate" type="text" class="Wdate" onclick="WdatePicker();" readonly="readonly" />
							</td>
							<td class="mRsTitle">记账时间：</td>
							<td>
								<input name="prListBuluQuery.beginGlDate" type="text" class="Wdate" onclick="WdatePicker();" readonly="readonly" />
								<var>-</var>
								<input name="prListBuluQuery.endGlDate" type="text" class="Wdate" onclick="WdatePicker();" readonly="readonly" />
							</td>
							<td class="mRsTitle">摊销时间：</td>
							<td>
								<input name="prListBuluQuery.amortizeBegin" value="2013-01-01" type="text" class="Wdate" onclick="WdatePicker();" readonly="readonly" />
								<var>-</var>
								<input name="prListBuluQuery.amortizeEnd" value="2049-12-31" type="text" class="Wdate" onclick="WdatePicker();" readonly="readonly" />
							</td>
						</tr>
						<tr>
							<td class="mRsTitle" id="bizlineTd">业务线：</td>
							<td id="bizlineTd2">
								<s:select multiple="true" id="search_BUSINESS_LINE" name="prLineSelect"  list="bizLineMap.get(#request.prLine)" listKey="llCode" listValue="llName" ></s:select>
								<s:select id="search_BUSINESS_LINE_hidden_" list="bizLineMap.get(#request.prLine)" listKey="%{llParentCode + '@' + llCode}" listValue="llName" cssStyle="display:none;"></s:select>
							</td>
							<td class="mRsTitle">费用大类：</td>
							<td>
								<s:select multiple="true" id="search_EXPENSE_TYPE" name="prTypeSelect"  list="expenseTypeMap.get(#request.expenseType)" listKey="llCode" listValue="llName"></s:select>
								<s:select id="search_EXPENSE_TYPE_hidden_BUSINESS_LINE" list="expenseTypeMap.get(#request.expenseType)" listKey="%{llParentCode + '@' + llCode}" listValue="llName" cssStyle="display:none;"></s:select>
							</td>
							<td class="mRsTitle">费用小类：</td>
							<td>
								<s:select multiple="true" id="search_EXPENSE_SUBTYPE" name="prSubTypeSelect"  list="expenseSubTypeMap.get(#request.subExpenseType)" listKey="llCode" listValue="llName"></s:select>
								<s:select id="search_EXPENSE_SUBTYPE_hidden_EXPENSE_TYPE" list="expenseSubTypeMap.get(#request.subExpenseType)" listKey="%{llParentCode + '@' + llCode}" listValue="llName" cssStyle="display:none;"></s:select>
							</td>
							<s:if test = "#request.model=='FOCUS'">
							<td class="mRsTitle">PR单号：</td>
							<td colspan="5">
								<input type="text" name="prListBuluQuery.purchaseNumber" />
							</td>
							</s:if>
						</tr>
						<s:if test = "#request.model!='FOCUS'">
						<tr>
							<td class="mRsTitle">PR单号：</td>
							<td colspan="5">
								<input type="text" name="prListBuluQuery.purchaseNumber" />
							</td>
						</tr>
						</s:if>
					</table>
					<input id="prLine" type="hidden" name="prListBuluQuery.prLine" value=""/>
					<input id="prType" type="hidden" name="prListBuluQuery.prType" value=""/>
					<input id="prSubType" type="hidden" name="prListBuluQuery.prSubType" value=""/>
				</div>
			</div>
				<input id="reset" type="reset" style="display: none" />
		</form>

		<div id="div_sub" class="seaSub">
			<a id="queryBtn" class="subUse subBtn" href="javascript:void(0);" onclick="check('query');return false;">查询</a>
			<a class="subRestBtn" href="javascript:void(0);" onclick="resetxForm('<s:property value='#request.model'/>');">重置</a>
		</div>

		<div class="JgopDes">
			<div id="div_summary" class="jGdesc">
				<strong>共计数据条数：</strong>
				<span class="numSp" id="count"></span>
				<strong>与PR系统同步时间：</strong>
				<span class="timeSp" id="syncDate"></span>
				<strong>最后修改人：</strong>
				<span class="editSp" id="syncPeople"></span>
			</div>
			<div id="div_operation" class="mRsarOp">
				<a id="syncBtn" href="javascript:void(0)" class="geyBtn" onclick="check('syncDate');return false;">数据同步</a>
				<!--    <a id="commitBtn" href="javascript:void(0)" class="geyBtn3">提交数据</a>  -->
				<a id="saveBtn" href="javascript:void(0)" class="geyBtn2" onclick="check('saveBtn');return false;">保 存</a>
				<a id="importBtn" href="javascript:void(0)" class="geyBtn" onclick="check('importExcel');return false;">从Excel导入</a>
				<a id="exportBtn" href="javascript:void(0)" class="geyBtn" onclick="check('exportExcel');return false;">导出Excel</a>
			</div>
		</div>

		<!-- JG列表 -->
		<div class="jGlist" z-index="1">
			<table id="userTable" class="userTable"></table>
		</div>
		<!-- 分页 -->
		<div id="jGpage"></div>
	</div>
	<div class="clear">&nbsp;</div>
</div>
<%@ include file="pr-excel.jsp"%>
</body>
</html>