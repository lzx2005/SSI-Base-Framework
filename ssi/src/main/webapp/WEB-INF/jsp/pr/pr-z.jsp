<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><s:property value="templateName" />_PR确认导入数据</title>
<%@ include file="/common/css-js.jsp"%>
<script type="text/javascript" src="${ctx}/js/pr.js"></script>
<%@ include file="/common/css-js-multiselect.jsp"%>
<script type="text/javascript">

//取得编辑选项
function getEditOptions(){
	return {value:"${selectOptions}","class":"jqgrid-row-editing-select jgedit-select"};
}
	$(function() {
		initPageBgMapping();//设置原场景、版本、年
		var result = "<s:property value='#request.canSync'/>";
		//alert(model);
		var model = "<s:property value='#request.model'/>";
		var syncDate = "<s:property value='#request.syncDate'/>";
		var syncPeople = "<s:property value='#request.syncPeople'/>";
		//switchBgMapping(true,model,"z",ctx);
		if (syncDate.length != 0) {
			$("#syncDate").html(syncDate);
		}
		if (syncPeople.length != 0) {
			$("#syncPeople").html(syncPeople);
		}
		
		initUpdateDateForZ();
		
		if (model == "FOCUS") {
			$("#bizlineTd").hide();
			$("#bizlineTd2").hide();
		}
		
        changeType(model);//设置大小类
     	//版本是未开启状态时，不显示修改人、修改时间
		hiddenUpdateUserAndTime();
		//projcectValue_select();////保存原project值
		
		if (result == "true") {
			//changeProjectForOption('init');
			//gridFun();
			gridFun();
			normalBtn(); 
			optionNone();//根据场景版本状态，锁定按钮
			changeProjectForOption();
			//自动提示 项目1 2 值集
			changeProjectForAuto();
		} else {
			disableBtn();
			//alert("正在同步中,同步，提交数据，保存等操作不可用！");
			sycnMsgDialog($("#syncPeople").html(),'init');
		}
		resizeGrid("userTable");
	});
	/**
	 * z 界面 更新时间
	 */
	function initUpdateDateForZ(){
		$.post("${ctx}/pr/queryTimeZ?model=<s:property value='#request.model'/>", function(data) {
			$("#syncDate").html(data.lastUpdateDate);
			$("#syncPeople").html(data.lastUpdateUser);
		}, 'json'); 
	}
	//z不需要判断同步状态
	function check(param) {
		//判断场景版本是否切换  
		if(isChangeVersion()){
			if(window.confirm("您已改变了场景版本,是否切换?")){
				projcectValue_select();//保存 project值
				$("#switchBgMapping").trigger("click");
				return;
			}
			else{//修改场景版本信息  稍候做
				backVersion();
			}
		}
		//查询、导出操作  判断是否之前存在删除记录、未保存
		if (param == "query"||param == "exportExcel") {
			if((isExitDel||isExitSaveOption())&&!window.confirm("当前界面模版中有为保存的数据,是否真的要离开界面?")){
				return;
			}
		}
		
		if (param == "saveBtn") {
			saveBtn();
		} else if (param == "query") {
			query();
		} else if (param == "delBtn") {
			delBtn();
		} else if (param == "exportExcel") {
			exportExcel();
		}
	}
	/**
	 * 删除
	 */
	function delBtn() {
		//1,
		var model = "<s:property value='#request.model'/>";
		var yearName = $("#bg_year").val();
		var sence = $("#bg_sence").val();
		var version = $("#bg_version").val();
		var selectedIds = jQuery("#userTable").jqGrid('getGridParam', 'selarrrow');
		if (selectedIds.length == 0) {
			alert("请至少选择1条数据!");
			return;
		}
		var canWrite = $("#canWrite").val();
		if (canWrite != "1") {
			alert("【" + yearName + "】年--【" + sence + "】场景--【" + version + "】版本,状态为不可录入，请切换版本!");
			return;
		}
		if(!window.confirm("选中的数据将会从["+page_year+"年]["+page_sence+"场景]["+page_version+"版本]中删除，且需要点击[保存]按钮才能生效，确认删除吗?")){
			return;
		}
		/* if (!confirm("对选中的记录将从【" + yearName + "】年--【" + sence + "】场景--【" + version + "】版本下进行删除,是否确认?")) {
			return;
		}  */

		var allIds = jQuery("#userTable").jqGrid('getDataIDs');//getallIds
		//编辑保存列前，执行一遍假保存.make sure cell is ok!
		for ( var i = 0; i < allIds.length; i++) {
			jQuery('#userTable').jqGrid('saveRow', allIds[i]);
		}
		//var saveIds = minus(allIds, selectedIds);
		var jsonArr = "";
		for ( var i = 0; i < selectedIds.length; i++) {
			//排除 对本页合计行 与 查询结果合计行  操作
			if(selectedIds[i]==201301||selectedIds[i]==201302){
				continue;
			}
			var ret = jQuery("#userTable").jqGrid('getRowData', selectedIds[i]);
			jsonArr += "{model:\"" + model + "\",purchaseNumber:\"" + ret.purchase_number + "\",bizline:\"" + ret.bizline + "\",expenseType:\"" + ret.expenseType + "\",expenseSubtype:\""
					+ ret.expenseSubtype +"\",yearName:\"" + yearName + "\",sence:\"" + sence + "\",version:\"" + version
					+ "\"},";
		}
		
		//删除行
		for ( var i = 0; i < selectedIds.length; i++) {
			//排除 对本页合计行 与 查询结果合计行  操作
			if(selectedIds[i]==201301||selectedIds[i]==201302){
				continue;
			}
			$("#userTable").jqGrid('delRowData', selectedIds[i]);
			i--;
		}
		
		$("tr[id=201301]>td:lt(2)", $("#userTable")).html("");
		$("tr[id=201301]>td:lt(2)", $("#userTable_frozen")).html("");
		
		$("tr[id=201302]>td:lt(2)", $("#userTable")).html("");
		$("tr[id=201302]>td:lt(2)", $("#userTable_frozen")).html("");
		
		jsonArr_del += jsonArr;
		
		if(!isExitDel&&jsonArr_del.length>0){
			isExitDel = true;//判断是否存在删除数据  false 不存在
		}
		/* 
		var jsonArr = "[";
		for ( var i = 0; i < selectedIds.length; i++) {
			//排除 对本页合计行 与 查询结果合计行  操作
			if(selectedIds[i]==201301||selectedIds[i]==201302){
				continue;
			}
			var ret = jQuery("#userTable").jqGrid('getRowData', selectedIds[i]);
			jsonArr += "{model:\"" + model + "\",purchaseNumber:\"" + ret.purchase_number + "\",bizline:\"" + ret.bizline + "\",expenseType:\"" + ret.expenseType + "\",expenseSubtype:\""
					+ ret.expenseSubtype +"\",yearName:\"" + yearName + "\",sence:\"" + sence + "\",version:\"" + version
					+ "\"},";
		}
		jsonArr= jsonArr.substring(0, jsonArr.lastIndexOf(",")) + "]";
		showMaskLayer();
		var params = $("#xForm").serialize();
		$.post("${ctx}/pr/editDel?viewName=z&"+params, "jsonArrStr=" + encodeURIComponent(jsonArr), function(data, state) {
			if (data == "OK") {
				query();
				closeMaskLayer();
				changeProjectForOption();
				initUpdateDateForZ();
				alert("删除成功!");
			} else {
				closeMaskLayer();
				alert("删除失败!");
			}
		}); */
	}
	/**
	 * 查询
	 */
	function query() {
		projcectValue_select();//保存 project值
		
		isExitDel = false;//判断是否存在删除数据  false 不存在
		jsonArr_del = '';//待删除数据
		
		jQuery("#userTable").setGridParam({postData:null});//清除之前表单postDate数据，防止重复查询
		
		setTypeForQuery();//保存 业务线 费用大小类值 查询使用
		var url = "${ctx}/pr/query?viewName=z&model=<s:property value='#request.model'/>&time="+new Date().getTime();
		
		jQuery("#userTable").setGridParam({
			url : url,
			postData:$("#xForm").serializeObject(), //发送数据
			page:1
		}).trigger("reloadGrid");
		changeProjectForOption();
	}

	/**
	 * grid保存
	 */
	function saveBtn() {
		var model = "<s:property value='#request.model'/>";
		var yearName = $("#bg_year").val();
		var sence = $("#bg_sence").val();
		var version = $("#bg_version").val();
		var selectedIds = jQuery("#userTable").jqGrid('getGridParam', 'selarrrow');
		if (selectedIds.length == 0&&!isExitDel) {
			alert("请至少选择1条数据!");
			return;
		}

		var canWrite = $("#canWrite").val();
		if (canWrite != "1") {
			alert("【" + yearName + "】年--【" + sence + "】场景--【" + version + "】版本,状态为不可录入，请切换版本!");
			return;
		}
		//编辑保存列前，执行一遍假保存.make sure cell is ok!
		var dataIds = jQuery('#userTable').jqGrid('getDataIDs');
		for ( var i = 0; i < dataIds.length; i++) {
			jQuery('#userTable').jqGrid('saveRow', dataIds[i]);
		}
		if('<s:property value='#request.model'/>'=='FOCUS'&&isExitSaveOption()){
			
			for ( var i = 0; i < selectedIds.length; i++) {
				if(selectedIds[i]==201301||selectedIds[i]==201302){
					continue;
				}
				var ret = jQuery("#userTable").jqGrid('getRowData', selectedIds[i]);
				//alert(ret.project1);
				if(ret.projectName1.length==0){
					flag=true;
					alert("项目1不能为空，请重新填写");
					return;
				}
			}
		}
/* 		if (!confirm("是否确认保存所选记录么?")) {
			return;
		} */
		if(!window.confirm("数据将被保存在["+yearName+"年]["+sence+"场景]["+version+"版本]下,确认保存?")){
			return;
		}
		//编辑保存列前，执行一遍假保存.make sure cell is ok!
		var dataIds = jQuery('#userTable').jqGrid('getDataIDs');
		for ( var i = 0; i < dataIds.length; i++) {
			jQuery('#userTable').jqGrid('saveRow', dataIds[i]);
		}
		var jsonArr = "[";
		for ( var i = 0; i < selectedIds.length; i++) {
			//排除 对本页合计行 与 查询结果合计行  操作
			if(selectedIds[i]==201301||selectedIds[i]==201302){
				continue;
			}
			var ret = jQuery("#userTable").jqGrid('getRowData', selectedIds[i]);
			//jsonArr+=JSON.stringify(ret)+","; or jsonArr+=JsonToStr(ret);
			var project1 = ret.projectName1;
			/* if(page_model=='FOCUS'&&ret.projectName1=='请选择'){
				project1 = '';
			} */
			jsonArr += "{model:\"" + model + "\",purchaseNumber:\"" + ret.purchase_number + "\",bizline:\"" + ret.bizline + "\",expenseType:\"" + ret.expenseType + "\",expenseSubtype:\""
			+ ret.expenseSubtype + "\",yearName:\"" + yearName + "\",sence:\"" + sence + "\",version:\"" + version+ "\",isDel:\"0\",ps:\"" + ret.ps+ "\",project1:\""+project1+"\",project2:\""+ret.projectName2+"\"},";
		}
		if(jsonArr == "["){
			jsonArr += "]";
		}
		else{
			jsonArr = jsonArr.substring(0, jsonArr.lastIndexOf(",")) + "]";
		}
		showMaskLayer();
		
		if(isExitDel&&jsonArr_del.length>0){
			jsonArr_del = jsonArr_del.substring(0, jsonArr_del.lastIndexOf(","));
		}		
		jsonArr_del = '['+jsonArr_del+']';
		$.post("${ctx}/pr/editSave?viewName=z",
				{'jsonArrStr':encodeURIComponent(jsonArr),'jsonArrStrDel':encodeURIComponent(jsonArr_del)} , 
				function(data, state) {
			if (data == "OK") {
				query();
				closeMaskLayer();
				changeProjectForOption();
				initUpdateDateForZ();
				//自动提示 项目1 2 值集
				changeProjectForAuto();
				project1Arr=[];
		    	project2Arr=[];
				alert("保存成功!");
				
			} else {
				closeMaskLayer();
				alert("保存失败!");
			}
		});
	}
	/**
	 * grid加载
	 */
	function gridFun() {
		setTypeForQuery();//保存 业务线 费用大小类值 查询使用
		jQuery("#userTable").jqGrid({
			url : "${ctx}/pr/query?viewName=z&model=<s:property value='#request.model'/>&t="+new Date().getTime(),
			datatype : "json",
			mtype : "POST",
			postData:$("#xForm").serializeObject(),
			colNames : ['index','<span style=\'color:red;\'>审核日期</span>','<span style=\'color:red;\'>PR单号</span>','PR单说明',<s:if test="#request.model=='FOCUS'">'<span style=\'color:red;\'>项目1</span>'</s:if><s:else>'项目1'</s:else>,'项目2','供应商','部门','<span style=\'color:red;\'>业务线</span>','<span style=\'color:red;\'>费用大类</span>',<s:if test="#request.model=='SOHU'||#request.model=='FOCUS'">'<span style=\'color:red;\'>费用小类</span>'</s:if><s:else>'费用小类'</s:else>,'<span style=\'color:red;\'>申请金额</span>','服务范围From','服务范围To','财务备注','上一年度10月','上一年度11月','上一年度12月','以前年度合计','1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月','本年合计','下一年度1月','下一年度2月','下一年度3月','下一年度4月','下一年度5月','下一年度6月','下一年度7月','下一年度8月','下一年度9月','下一年度10月','下一年度11月','下一年度12月','下一年度合计','备注'],
			colModel : [ {name : 'index', index : 'index', hidden:true, frozen:true},
			             {name : 'approvalDate', index : 'approval_date',frozen:true},
			             {name : 'purchase_number', index : 'purchase_number'},//purchase_number
			             {name : 'purchaseName', index : 'PURCHASE_NAME'},
			             {name : 'projectName1', index : 'PROJECT_NAME1',<s:if test="#request.model=='AUTO'">editable:false</s:if><s:else>editable:true</s:else><s:if test="#request.model=='FOCUS'">,edittype:"select",editoptions:getEditOptions()</s:if>},//焦点是options
			             {name : 'projectName2', index : 'PROJECT_NAME2',<s:if test="#request.model=='AUTO'">editable:false</s:if><s:else>editable:true</s:else>},//project2
			             {name : 'vendorName', index : 'VENDOR_NAME'},
			             {name : 'deptName', index : 'DEPT_NAME'},
			             {name : 'bizline', index : 'BUSINESS_LINE'},//bizline
			             {name : 'expenseType', index : 'EXPENSE_TYPE'},//expenseType
			             {name : 'expenseSubtype', index : 'EXPENSE_SUBTYPE'},//expenseSubtype
			             {name : 'purchaseAmount', index : 'PURCHASE_AMOUNT',align : 'right',formatter: 'currency'},
			             {name : 'serviceBeginDate', index : 'SERVICE_BEGIN_DATE',formatter:'date', formatoptions:{newformat:'Y-m-d'}},
			             {name : 'serviceEndDate', index : 'SERVICE_END_DATE',formatter:'date', formatoptions:{newformat:'Y-m-d'}},
			             {name : 'approverDesc', index : 'APPROVER_DESC'},
			             {name : 'last10', index : 'LAST_10',align : 'right',formatter: 'currency'},
			             {name : 'last11', index : 'LAST_11',align : 'right',formatter: 'currency'},
			             {name : 'last12', index : 'LAST_12',align : 'right',formatter: 'currency'},
			             {name : 'lastQ4', index : 'LAST_Q4',align : 'right',formatter: 'currency'},
			             {name : 'this01', index : 'this_01',align : 'right',formatter: 'currency'},
			             {name : 'this02', index : 'this_02',align : 'right',formatter: 'currency'},
			             {name : 'this03', index : 'this_03',align : 'right',formatter: 'currency'},
			             {name : 'this04', index : 'this_04',align : 'right',formatter: 'currency'},
			             {name : 'this05', index : 'this_05',align : 'right',formatter: 'currency'},
			             {name : 'this06', index : 'this_06',align : 'right',formatter: 'currency'},
			             {name : 'this07', index : 'this_07',align : 'right',formatter: 'currency'},
			             {name : 'this08', index : 'this_08',align : 'right',formatter: 'currency'},
			             {name : 'this09', index : 'this_09',align : 'right',formatter: 'currency'},
			             {name : 'this10', index : 'this_10',align : 'right',formatter: 'currency'},
			             {name : 'this11', index : 'this_11',align : 'right',formatter: 'currency'},
			             {name : 'this12', index : 'this_12',align : 'right',formatter: 'currency'},
			             {name : 'thisTotal', index : 'THIS_TOTAL',align : 'right',formatter: 'currency'},
			             {name : 'next01', index : 'next_01',align : 'right',formatter: 'currency'},
			             {name : 'next02', index : 'next_02',align : 'right',formatter: 'currency'},
			             {name : 'next03', index : 'next_03',align : 'right',formatter: 'currency'},
			             {name : 'next04', index : 'next_04',align : 'right',formatter: 'currency'},
			             {name : 'next05', index : 'next_05',align : 'right',formatter: 'currency'},
			             {name : 'next06', index : 'next_06',align : 'right',formatter: 'currency'},
			             {name : 'next07', index : 'next_07',align : 'right',formatter: 'currency'},
			             {name : 'next08', index : 'next_08',align : 'right',formatter: 'currency'},
			             {name : 'next09', index : 'next_09',align : 'right',formatter: 'currency'},
			             {name : 'next10', index : 'next_10',align : 'right',formatter: 'currency'},
			             {name : 'next11', index : 'next_11',align : 'right',formatter: 'currency'},
			             {name : 'next12', index : 'next_12',align : 'right',formatter: 'currency'},
			             {name : 'nextTotal', index : 'NEXT_TOTAL',align : 'right',formatter: 'currency'},
			             {name : 'ps', index : 'ps',editable:true}
			           ],
			rowNum : 10,
			rowList : [ 10, 15, 20 ], //可调整每页显示的记录数 
			pager : '#jGpage',
			rownumbers : true,
			//cellEdit:true,//单元格可编辑
			multiselect : true,//多选
			cellurl : "clientArray",//不会向server端触发请求
			editurl : "clientArray",//不会向server端触发请求
			sortname : 'approval_date',
			sortable:false,
			sortorder : "desc",
			autowidth : true,
			height : '428',
			// fixed:false,
			shrinkToFit : false,
			loadComplete : function(data) {
				//focus隐藏业务线
				var model = "<s:property value='#request.model'/>";
				if (model == "FOCUS") {
					$("#userTable").jqGrid('hideCol', "bizline");
				}
				var records =$("#userTable").jqGrid('getGridParam','records');//获取当前jqGrid的总记录数；
				$("#count").html(records);
				generateSumDataForPr(data);
				
				noResultMessage("userTable");
			},onCellSelect : function(rowid, index, contents, event) {//当点击单元格时触发行选中
				//var t=$("#userTable").jqGrid('setSelection',rowid);
				//alert("rowid="+rowid);alert("index="+index);alert("contents="+contents);
				if(rowid==201301||rowid==201302) return;
				
				$('#userTable').jqGrid('setSelection', rowid, false);
				//汽车 项目1 项目2 只读
				if((page_model!= "AUTO" &&(index==7||index==6||index==47))||(page_model== "AUTO" &&index==47)){
					
					$('#userTable').jqGrid('editRow', rowid, false);
					
					var project_1 = ''+rowid+'_projectName1';
					var project_2 = ''+rowid+'_projectName2';
					var remark_1 = ''+rowid+'_ps';
					$('#'+project_1).change(function(){
				        //alert($(this).val());  //弹出select的值
				        var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_'));
				        if(!checkRowStatus(row_id)){
				        	$('#userTable').jqGrid('setSelection', row_id, false);
				        }
				    });
					$('#'+project_2).change(function(){
				        //alert($(this).val());  //弹出select的值
						var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_'));
				        if(!checkRowStatus(row_id)){
				        	$('#userTable').jqGrid('setSelection', row_id, false);
				        }
				    });
					$('#'+remark_1).change(function(){
				        //alert($(this).val());  //弹出select的值
						var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_'));
				        if(!checkRowStatus(row_id)){
				        	$('#userTable').jqGrid('setSelection', row_id, false);
				        }
				    });
					//自动提示初始化
					initAutoCompletion();
				    //自动提示
					autoCompletion($("#"+rowid),"z",rowid);
				}else if(index==1){
					$('#userTable').jqGrid('setSelection', rowid, false);
				}
			}/* ,onSelectRow:function(rowid,state){
										alert(state);
										var t=$("#userTable").jqGrid('setSelection',rowid);
						            } */

		});
		jQuery("#userTable").jqGrid('setFrozenColumns');
	}
	
	// 生成合计内容
	function generateSumDataForPr(data){
		if(data.rows.length == 0)
			return;
		var data_1 ={'index':"",
			        'approvalDate':"<b>本页合计</b>",
			        'purchase_number':"",
			        'purchaseName':"",
			        'projectName1':"",
			        'projectName2':"",
			        'vendorName':"",
			        'deptName':"",
			        'bizline':"",
			        'expenseType':"",
			        'expenseSubtype':"",
			        'purchaseAmount':data.page_pur,
			        'serviceBeginDate':"",
			        'serviceEndDate':"",
			        'approverDesc':"",
			        'last10':data.page_last10,
			        'last11':data.page_last11,
			        'last12':data.page_last12,
			        'lastQ4':data.page_lastTotal,
			        'this01':data.page_this01,
			        'this02':data.page_this02,
			        'this03':data.page_this03,
			        'this04':data.page_this04,
			        'this05':data.page_this05,
			        'this06':data.page_this06,
			        'this07':data.page_this07,
			        'this08':data.page_this08,
			        'this09':data.page_this09,
			        'this10':data.page_this10,
			        'this11':data.page_this11,
			        'this12':data.page_this12,
			        'thisTotal':data.page_thisTotal,
			        'next01':data.page_next01,
			        'next02':data.page_next02,
			        'next03':data.page_next03,
			        'next04':data.page_next04,
			        'next05':data.page_next05,
			        'next06':data.page_next06,
			        'next07':data.page_next07,
			        'next08':data.page_next08,
			        'next09':data.page_next09,
			        'next10':data.page_next10,
			        'next11':data.page_next11,
			        'next12':data.page_next12,
			        'nextTotal':data.page_nextTotal,
			        'ps':""};
		
		var data_2 ={'index':"",
		        'approvalDate':"<b>查询结果合计</b>",
		        'purchase_number':"",
		        'purchaseName':"",
		        'projectName1':"",
		        'projectName2':"",
		        'vendorName':"",
		        'deptName':"",
		        'bizline':"",
		        'expenseType':"",
		        'expenseSubtype':"",
		        'purchaseAmount':data.total_pur,
		        'serviceBeginDate':"",
		        'serviceEndDate':"",
		        'approverDesc':"",
		        'last10':data.total_last10,
		        'last11':data.total_last11,
		        'last12':data.total_last12,
		        'lastQ4':data.total_lastTotal,
		        'this01':data.total_this01,
		        'this02':data.total_this02,
		        'this03':data.total_this03,
		        'this04':data.total_this04,
		        'this05':data.total_this05,
		        'this06':data.total_this06,
		        'this07':data.total_this07,
		        'this08':data.total_this08,
		        'this09':data.total_this09,
		        'this10':data.total_this10,
		        'this11':data.total_this11,
		        'this12':data.total_this12,
		        'thisTotal':data.total_thisTotal,
		        'next01':data.total_next01,
		        'next02':data.total_next02,
		        'next03':data.total_next03,
		        'next04':data.total_next04,
		        'next05':data.total_next05,
		        'next06':data.total_next06,
		        'next07':data.total_next07,
		        'next08':data.total_next08,
		        'next09':data.total_next09,
		        'next10':data.total_next10,
		        'next11':data.total_next11,
		        'next12':data.total_next12,
		        'nextTotal':data.total_nextTotal,
		        'ps':""};
        jQuery('#userTable').jqGrid('addRowData', 201301, data_1, 'last');
		jQuery('#userTable').jqGrid('addRowData', 201302, data_2, '');
		
		$("tr[id=201301]>td:lt(2)", $("#userTable")).html("");
		$("tr[id=201301]>td:lt(2)", $("#userTable_frozen")).html("");
		
		$("tr[id=201302]>td:lt(2)", $("#userTable")).html("");
		$("tr[id=201302]>td:lt(2)", $("#userTable_frozen")).html("");
	}
</script>
</head>
<body>
<div style="display:none">
	<s:select id="project1_Auto" list="project1Map"  headerKey="" headerValue="请选择"/>
	<s:select id="project2_Auto" list="project2Map"  headerKey="" headerValue="请选择"/>
</div>
<div class="mainCont">
	<div class="mainRight">
		<div class="mTitleBig">
			<h3>
				<span><s:property value="templateName" />_PR确认导入数据</span>
				<s:if test="template.temTip != null && template.temTip != ''"><a title="查看操作提示" href="${ctx}/commontemplate/showhelp?templateCode=${template.temCode}" target="_blank"><span class="tipSpan"></span></a>
				</s:if>
			</h3>
			<div class="clear">&nbsp;</div>
		</div>
		<form id="xForm" method="post" action="${ctx}/pr/export?viewName=z&model=<s:property value='#request.model'/>">
			<input  id="bg_model" name="prListBuluQuery.model" value="<s:property value='#request.model'/>" type="hidden" />
			<input  id="bg_viewName"  value="z" type="hidden" />
			<div id="div_bg" class="mRcont">
				<div class="mTitle">
					<h4>年度、场景、版本选择</h4>
					<div class="mRsearchGen">
						<span>年：</span>
						<s:select id="bg_year" list="{2013,2014}" name="prListBuluQuery.yearName"></s:select>
						<span>场景：</span>
						<s:select id="bg_sence" list="{'Q1Fcst', 'Q1Latest', 'Q2Fcst', 'Q2Latest', 'Q3Fcst', 'Q3Latest', 'Q4Fcst', 'Q4Latest', 'AnnualBudget', 'Actual'}" name="prListBuluQuery.sence"></s:select>
						<span>版本：</span>
						<s:select id="bg_version" list="{'V1', 'V2', 'V3', 'Final', 'Test'}" name="prListBuluQuery.version"></s:select>
						<span>状态：</span>
						<span class="mRMap" id="span_bg_allow">${prListBuluQuery.allow == 0 ? '已锁定' : (prListBuluQuery.allow == 1 ? '可录入' : '未开启')}</span>
					<span id="span_bg_allow_pic" class="${prListBuluQuery.allow == 0 ? 'allow0' : (prListBuluQuery.allow == 1 ? 'allow1' : 'allow2')}"></span>
						<input type="hidden" id="canWrite" value="${prListBuluQuery.allow}" />
					<a id="switchBgMapping" class="subUse subBtn" href="javascript:void(0);" onclick="switchBgMapping(false,'<s:property value="#request.model"/>','z',ctx);initUpdateDateForZ();">切换</a>
					</div>
					<div class="clear">&nbsp;</div>
				</div>
			</div>

			<div id="div_search" class="mRcont">
				<div class="mTitle" style="background: none;">
					<h4>
						查询条件选择&nbsp;
						<a href="javascript:toggleSearchArea();">显示/折叠查询区域</a>
					</h4>
					<div class="clear">&nbsp;</div>
				</div>
				<div class="mRsearch" id="div_search_cond" style="display: none">
					<table>
						<tr>
							<td class="mRsTitle">审核日期：</td>
							<td colspan="5">
								<input name="prListBuluQuery.approvalBegindate" type="text" class="Wdate" onclick="WdatePicker();" readonly="readonly" />
								<var>-</var>
								<input name="prListBuluQuery.approvalEnddate" type="text" class="Wdate" onclick="WdatePicker();" readonly="readonly" />
							</td>
						</tr>
						<tr>
							<td class="mRsTitle" id="bizlineTd">业务线：</td>
							<td id="bizlineTd2">
								<s:select multiple="true" id="search_BUSINESS_LINE" name="prLineSelect" list="bizLineMap.get(#request.prLine)" listKey="llCode" listValue="llName"></s:select>
								<s:select id="search_BUSINESS_LINE_hidden_" list="bizLineMap.get(#request.prLine)" listKey="%{llParentCode + '@' + llCode}" listValue="llName" cssStyle="display:none;"></s:select>
							</td>
							<td class="mRsTitle">费用大类：</td>
							<td>
								<s:select multiple="true" id="search_EXPENSE_TYPE" name="prTypeSelect"  list="expenseTypeMap.get(#request.expenseType)" listKey="llCode" listValue="llName"></s:select>
								<s:select id="search_EXPENSE_TYPE_hidden_BUSINESS_LINE" list="expenseTypeMap.get(#request.expenseType)" listKey="%{llParentCode + '@' + llCode}" listValue="llName" cssStyle="display:none;" ></s:select>
							</td>
							<td class="mRsTitle">费用小类：</td>
							<td>
								<s:select multiple="true" id="search_EXPENSE_SUBTYPE" name="prSubTypeSelect" list="expenseSubTypeMap.get(#request.subExpenseType)" listKey="llCode" listValue="llName"></s:select>
								<s:select id="search_EXPENSE_SUBTYPE_hidden_EXPENSE_TYPE" list="expenseSubTypeMap.get(#request.subExpenseType)" listKey="%{llParentCode + '@' + llCode}" listValue="llName" cssStyle="display:none;"></s:select>
							</td>
						</tr>
						<tr>
							<td class="mRsTitle">PR单号：</td>
							<td>
								<input type="text" name="prListBuluQuery.purchaseNumber" />
							</td>
							<td class="mRsTitle">项目1：</td>
							<td>
								<s:select style="width:200px" name="prListBuluQuery.project1" id="project1" list="project1Map" headerKey="" headerValue="请选择"/>
							</td>
							<td class="mRsTitle">项目2：</td>
							<td>
								<s:select style="width:200px" name="prListBuluQuery.project2" id="project2" list="project2Map"  headerKey="" headerValue="请选择"/>
							</td>
						</tr>
						
					</table>
					<input id="prLine" type="hidden" name="prListBuluQuery.prLine" value=""/>
					<input id="prType" type="hidden" name="prListBuluQuery.prType" value=""/>
					<input id="prSubType" type="hidden" name="prListBuluQuery.prSubType" value=""/>
				</div>
			</div>
			<input id="reset" type="reset" style="display: none" />
		</form>

		<div id="div_sub" class="seaSub">
			<a id="queryBtn" class="subUse subBtn" href="javascript:void(0);" onclick="check('query');return false;">查询</a>
			<a class="subRestBtn" href="javascript:void(0);" onclick="resetxForm('<s:property value='#request.model'/>');">重置</a>
		</div>

		<div class="JgopDes">
			<div id="div_summary" class="jGdesc">
				<strong>共计数据条数：</strong>
				<span class="numSp" id="count"></span>
				<strong>最近更新时间：</strong>
				<span class="timeSp" id="syncDate"></span>
				<strong>最后修改人：</strong>
				<span class="editSp" id="syncPeople"></span>
			</div>
			<div id="div_summary" class="mRsarOp">
				<a id="delBtn" href="javascript:void(0)" class="geyBtn2" onclick="check('delBtn');return false;">删除</a>
				<a id="saveBtn" href="javascript:void(0)" class="geyBtn2" onclick="check('saveBtn');return false;">保存</a>
				<a id="exportBtn" href="javascript:void(0)" class="geyBtn" onclick="check('exportExcel');return false;">导出Excel</a>
			</div>
		</div>

	<!-- JG列表 -->
	<div class="jGlist">
		<table id="userTable" class="userTable"></table>
	</div>
	<!-- 分页 -->
	<div id="jGpage"></div>

	</div>
	<div class="clear">&nbsp;</div>
</div>

</body>
</html>