<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>BI系统-excel导入</title>
<%@ include file="/common/css-js.jsp"%> 
<script>
	$(function(){
		var options = {
				type:"POST",
				dataType:"json",
				async:true,
		        url:"${ctx}/pr/importExcel?yearName=<s:property value='yearName'/>&sence=<s:property value='sence'/>&version=<s:property value='version'/>&viewName=<s:property value='viewName'/>&model=<s:property value='model'/>&"+$("#myForm").formSerialize(),
				beforeSubmit:check,
				success:endShow
		    };
		$("#myForm").bind('submit', function(){
			$(this).ajaxSubmit(options);
	        return false;//必须添加return false;语句 防止Form真实提交
	    });
    });	
	function check(){
		$("#result").hide(); 
		var myexcel=$("#myexcel").val();
		if(myexcel.length==0){
			alert("请选择文件！");
			return false;
		}
		var end=myexcel.substring(myexcel.lastIndexOf("\.")+1,myexcel.length);
		if(!(end=="xlsx"||end=="xls")){
			alert("该文件格式有误,请选择excel格式文件！");
			return false;
		}
	    return true;
	}
	function endShow(data){
		alert(data.message);
 		var type=data.type;
 		//alert(type);
 		if(type=="half"){
 			$("#result").html(""); 
 	 		$("#result").html("有问题的数据展示：<br/>"); 
 	 		 $.each(data.errorMap,function(key,value){   
 				    $("#result").append("第<font color='red'><b>"+key+"</b></font>行:")   
 				          .append("<font color='red'>"+value+"</font><br/>");   
 			}); 
 	 		 $("#result").slideToggle(2000); 
 	 	}
 		
	}
</script>
</head>

<body>
<center>
	<form id="myForm" action="${ctx}/pr/importExcel" method="post" enctype="multipart/form-data">
		<input type="file" id="myexcel"name="myexcel" accept="xls application/vnd.ms-excel,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
		<input type="submit" value="从excel导入"/>
	</form>
</center>
<div id="result" style="display:none">
错误显示区域：
</div>
</body>