<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><s:property value="templateName" />_PR修改数据</title>
<%@ include file="/common/css-js.jsp"%>
<script type="text/javascript" src="${ctx}/js/pr.js"></script>
<%@ include file="/common/css-js-multiselect.jsp"%>
<script type="text/javascript">
//取得编辑选项
function getEditOptions(){
	return {value:"${selectOptions}","class":"jqgrid-row-editing-select jgedit-select"};
}
	$(function() {
		initPageBgMapping();//设置原场景、版本、年
		
		var result = "<s:property value='#request.canSync'/>";
		var syncDate = "<s:property value='#request.syncDate'/>";
		var syncPeople = "<s:property value='#request.syncPeople'/>";
		var model = "<s:property value='#request.model'/>";
		
		if (syncDate.length != 0) {
			$("#syncDate").html(syncDate);
		}
		if (syncPeople.length != 0) {
			$("#syncPeople").html(syncPeople);
		}

		//switchBgMapping(true,model,"y",ctx);
        changeType(model);//初始化业务线、大小类
      	//版本是未开启状态时，不显示修改人、修改时间
		hiddenUpdateUserAndTime();
		//projcectValue_select();////保存原project值
		
		if (model == "FOCUS") {
			$("#bizlineTd").hide();
			$("#bizlineTd2").hide();
		}
		if (result == "true") {
			//changeProjectForOption('init');
			gridFun();
			normalBtn(); 
			optionNone()//根据场景版本状态，锁定按钮
			changeProjectForOption();
			//自动提示 项目1 2 值集
			changeProjectForAuto();
		} else {
			disableBtn();
			//alert("正在同步中,同步，提交数据，保存等操作不可用！");
			sycnMsgDialog($("#syncPeople").html(),'init');
		}
		resizeGrid("userTable");
		
	});

	//js method control
	function check(param) {
		$.post("${ctx}/pr/checkButtonForFullName?t=" + new Date().getTime()+"&model="+'<s:property value='#request.model'/>', function(data, state) {
			//alert(data);
			if (data.result == "ok") {
				//判断场景版本是否切换  
				if(isChangeVersion()){
					if(window.confirm("您已改变了场景版本,是否切换?")){
						projcectValue_select();//保存 project值
						$("#switchBgMapping").trigger("click");
						return;
					}
					else{//修改场景版本信息  稍候做
						backVersion();
					}
				} 
				//查询、导出操作  判断是否之前存在删除记录、未保存
				if (param == "query"||param == "exportExcel"||param == "importExcel"||param == "syncDate") {
					if(isExitSaveOption()&&!window.confirm("当前界面模版中有为保存的数据,是否真的要离开界面?")){
						return;
					}
				}
				if (param == "saveBtn") {
					saveBtn();
				} else if (param == "query") {
					query();
				} else if (param == "importExcel") {
					importExcel();
				} else if (param == "exportExcel") {
					exportExcel();
				} else if (param == "syncDate") {
					syncDate();
				}
			} else {
				disableBtn();
				//alert("正在执行同步请等候。。。");
				sycnMsgDialog(data.req_by,'');
			}
		});
	}
	/**
	 * 查询
	 */
	function query() {
		projcectValue_select();//保存 project值
		
		jQuery("#userTable").setGridParam({postData:null});//清除之前表单postDate数据，防止重复查询
		
		setTypeForQuery();//保存 业务线 费用大小类值 查询使用
		var params = $("#xForm").serializeObject();
		
		jQuery("#userTable").setGridParam({
			url : "${ctx}/pr/query?viewName=y&model=<s:property value='#request.model'/>&time="+new Date().getTime(),
			postData:params,//发送数据
			page : 1
		}).trigger("reloadGrid");
		
		changeProjectForOption();
	}
	/**
	 * 数据同步
	 */
	function syncDate() {
		if (!confirm("是否确认同步操作？")) {
			return;
		}
		//1设置本页面连接不可用-样式变化
		disableBtn();
		//调用后台sync函数
		$.post("${ctx}/pr/syncData?step=step1&viewName=y&model=<s:property value='#request.model'/>", function(data, state) {
			alert(data);
			if (state == "success") {
				$.post("${ctx}/pr/syncData?step=step2&viewName=y&model=<s:property value='#request.model'/>", function(data, state) {
					gridFun();
					//normalBtn(); 
					changeProjectForOption();
					//自动提示 项目1 2 值集
					changeProjectForAuto();
					project1Arr=[];
			    	project2Arr=[];
					alert(data);
				});
			}
		});
		normalBtn();
	}

	/**
	 * grid保存
	 */
	function saveBtn() {
		var model = "<s:property value='#request.model'/>";
		var yearName = $("#bg_year").val();
		var sence = $("#bg_sence").val();
		var version = $("#bg_version").val();
		var selectedIds = jQuery("#userTable").jqGrid('getGridParam', 'selarrrow');
		if (selectedIds.length == 0) {
			alert("请至少选择1条数据!");
			return;
		}
		var canWrite = $("#canWrite").val();
		if (canWrite != "1") {
			alert("【" + yearName + "】年--【" + sence + "】场景--【" + version + "】版本,状态为不可录入，请切换版本!");
			return;
		}
		//编辑保存列前，执行一遍假保存.make sure cell is ok!
		var dataIds = jQuery('#userTable').jqGrid('getDataIDs');
		for ( var i = 0; i < dataIds.length; i++) {
			jQuery('#userTable').jqGrid('saveRow', dataIds[i]);
		}
		
		if('<s:property value='#request.model'/>'=='FOCUS'){
			
			for ( var i = 0; i < selectedIds.length; i++) {
				if(selectedIds[i]==201301||selectedIds[i]==201302){
					continue;
				}
				var ret = jQuery("#userTable").jqGrid('getRowData', selectedIds[i]);
				//alert(ret.project1);
				if(ret.project1.length==0){
					flag=true;
					alert("项目1不能为空，请重新填写");
					return;
				}
			}
		}
		
		/* if (!confirm("是否确认添加到【" + yearName + "】年--【" + sence + "】场景--【" + version + "】版本么?")) {
			return;
		} */
		if(!window.confirm("数据将被保存在["+yearName+"年]["+sence+"场景]["+version+"版本]下,确认保存?")){
			return;
		}
		var jsonArr = "[";
		for ( var i = 0; i < selectedIds.length; i++) {
			//排除 对本页合计行 与 查询结果合计行  操作
			if(selectedIds[i]==201301||selectedIds[i]==201302){
				continue;
			}
			var ret = jQuery("#userTable").jqGrid('getRowData', selectedIds[i]);
			//jsonArr+=JSON.stringify(ret)+","; or jsonArr+=JsonToStr(ret);
			var project1 = ret.project1;
			/* if(page_model=='FOCUS'&&ret.project1=='请选择'){
				project1 = '';
			} */
			jsonArr += "{model:\"" + model + "\",purchaseNumber:\"" + ret.purchase_number + "\",bizline:\"" + ret.bizline + "\",expenseType:\"" + ret.expense_type + "\",expenseSubtype:\""
					+ ret.expense_subtype + "\",project1:\"" + project1 + "\",project2:\"" + ret.project2 + "\",yearName:\"" + yearName + "\",sence:\"" + sence + "\",version:\"" +version+"\",flag:\"" +ret.flag+ "\",remark:\"" + ret.remark + "\"},";
		}
		jsonArr = jsonArr.substring(0, jsonArr.lastIndexOf(",")) + "]";
		showMaskLayer();
		$.post("${ctx}/pr/editSave?viewName=y", "jsonArrStr=" + encodeURIComponent(jsonArr), function(data, state) {
			if (data == "OK") {
				query();
				closeMaskLayer();
				changeProjectForOption();
				//自动提示 项目1 2 值集
				changeProjectForAuto();
				project1Arr=[];
		    	project2Arr=[];
				alert("保存成功!");
			} else {
				closeMaskLayer();
				alert("保存失败!");
			}
		});
	}
	/**
	 * grid加载
	 */
	function gridFun() {
		setTypeForQuery();//保存 业务线 费用大小类值 查询使用
		jQuery("#userTable").jqGrid({
			url : "${ctx}/pr/query?viewName=y&model=<s:property value='#request.model'/>&t=" + new Date().getTime(),
			datatype : "json",
			mtype : "POST",
			postData:$("#xForm").serializeObject(),
			colNames : ['index','<span style=\'color:red;\'>审核日期</span>','<span style=\'color:red;\'>PR单号</span>','PR单说明',<s:if test="#request.model=='FOCUS'">'<span style=\'color:red;\'>项目1</span>'</s:if><s:else>'项目1'</s:else>,'项目2','供应商','部门','<span style=\'color:red;\'>业务线</span>','<span style=\'color:red;\'>费用大类</span>',<s:if test="#request.model=='SOHU'||#request.model=='FOCUS'">'<span style=\'color:red;\'>费用小类</span>'</s:if><s:else>'费用小类'</s:else>,'<span style=\'color:red;\'>申请金额</span>','服务范围From','服务范围To','更新人','财务备注','上一年度10月','上一年度11月','上一年度12月','以前年度合计','1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月','本年合计','下一年度1月','下一年度2月','下一年度3月','下一年度4月','下一年度5月','下一年度6月','下一年度7月','下一年度8月','下一年度9月','下一年度10月','下一年度11月','下一年度12月','下一年度合计','备注','状态'],
			colModel : [{name : 'index', index : 'index',hidden:true,frozen:true},
			            {name : 'approval_date', index : 'APPROVAL_DATE',frozen:true},
			             {name : 'purchase_number', index : 'purchase_number'},
			             {name : 'purchase_name', index : 'PURCHASE_NAME'},
			             {name : 'project1', index : 'project1',<s:if test="#request.model=='AUTO'">editable:false</s:if><s:else>editable:true</s:else><s:if test="#request.model=='FOCUS'">,edittype:"select",editoptions:getEditOptions()</s:if>},//焦点是options
			             {name : 'project2', index : 'project2',<s:if test="#request.model=='AUTO'">editable:false</s:if><s:else>editable:true</s:else>},
			             {name : 'vendor_name', index : 'VENDOR_NAME'},
			             {name : 'dept', index : 'dept'},
			             {name : 'bizline', index : 'bizline'},
			             {name : 'expense_type', index : 'EXPENSE_TYPE'},
			             {name : 'expense_subtype', index : 'EXPENSE_SUBTYPE'},
			             {name : 'purchase_amount', index : 'PURCHASE_AMOUNT_Str',align : 'right'},
			             {name : 'service_begin_date', index : 'SERVICE_BEGIN_DATE'},
			             {name : 'service_end_date', index : 'SERVICE_END_DATE'},
			             {name : 'user_name', index : 'user_name'},
			             {name : 'approver_desc', index : 'APPROVER_DESC'},
			             {name : 'last_oct', index : 'last_Oct_Str',align : 'right'},
			             {name : 'last_nov', index : 'last_Nov_Str',align : 'right'},
			             {name : 'last_dec', index : 'last_Dec_Str',align : 'right'},
			             {name : 'last_year', index : 'last_Year_Str',align : 'right'},
			             {name : 'jan', index : 'jan_Str',align : 'right'},
			             {name : 'feb', index : 'feb_Str',align : 'right'},
			             {name : 'mar', index : 'mar_Str',align : 'right'},
			             {name : 'apr', index : 'apr_Str',align : 'right'},
			             {name : 'may', index : 'may_Str',align : 'right'},
			             {name : 'jun', index : 'jun_Str',align : 'right'},
			             {name : 'jul', index : 'jul_Str',align : 'right'},
			             {name : 'aug', index : 'aug_Str',align : 'right'},
			             {name : 'sep', index : 'sep_Str',align : 'right'},
			             {name : 'oct', index : 'oct_Str',align : 'right'},
			             {name : 'nov', index : 'nov_Str',align : 'right'},
			             {name : 'dec', index : 'dec_Str',align : 'right'},
			             {name : 'year', index : 'year_Str',align : 'right'},
			             {name : 'next_jan', index : 'next_Jan_Str',align : 'right'},
			             {name : 'next_feb', index : 'next_Feb_Str',align : 'right'},
			             {name : 'next_mar', index : 'next_Mar_Str',align : 'right'},
			             {name : 'next_apr', index : 'next_Apr_Str',align : 'right'},
			             {name : 'next_may', index : 'next_May_Str',align : 'right'},
			             {name : 'next_jun', index : 'next_Jun_Str',align : 'right'},
			             {name : 'next_jul', index : 'next_Jul_Str',align : 'right'},
			             {name : 'next_aug', index : 'next_Aug_Str',align : 'right'},
			             {name : 'next_sep', index : 'next_Sep_Str',align : 'right'},
			             {name : 'next_oct', index : 'next_Oct_Str',align : 'right'},
			             {name : 'next_nov', index : 'next_Nov_Str',align : 'right'},
			             {name : 'next_dec', index : 'next_Dec_Str',align : 'right'},
			             {name : 'next_year', index : 'next_Year_Str',align : 'right'},
			             {name : 'remark', index : 'remark',editable:true,sortable:false},
			             {name : 'flag', index : 'flag',hidden:true}
			           ],
			rowNum: 10,
			rowList : [ 10, 15, 20 ], //可调整每页显示的记录数 
			pager : '#jGpage',
			rownumbers : true,
			//cellEdit:true,//单元格可编辑
			multiselect : true,//多选
			cellurl : "clientArray",//不会向server端触发请求
			editurl : "clientArray",//不会向server端触发请求
			sortname : 'APPROVAL_DATE',
			sortable:false,
			sortorder : "desc",
			autowidth : true,
			height : '428',
			shrinkToFit : false,
			loadComplete : function(data) {
				var model = "<s:property value='#request.model'/>";
				var yearName = $("#bg_year").val();
				var sence = $("#bg_sence").val();
				var version = $("#bg_version").val();
				if (model == "FOCUS") {
					$("#userTable").jqGrid('hideCol', "bizline");
				}
				
				generateSumDataForPr(data);
				$("#count").html(data.records);//获取当前jqGrid的总记录数；
				//字段变色查询
				var dataIds = jQuery('#userTable').jqGrid('getDataIDs');
				//变化字段加红色底色.
				//已删除状态的变为蓝色
				
				if(dataIds.length>0){
					//var start = new Date().getTime();
					var jsonArr = "[";
					for ( var i = 0; i < dataIds.length; i++) {
						//排除 对本页合计行 与 查询结果合计行  操作
						if(dataIds[i]==201301||dataIds[i]==201302){
							continue;
						}
						var ret = jQuery("#userTable").jqGrid('getRowData', dataIds[i]);
						//ret.flag='已删除'
						if(ret.flag=='已删除'){//1.查找flag为已删除的记录 整行设置字体为 蓝色
							var tr=$("tr[id="+parseInt(i+1)+"]");
							tr.css("color","#0000FF");
						}
						 else{//2.组装串到后台查看 红色字体
							jsonArr += "{purchaseNumber:\"" + ret.purchase_number+ "\",expenseType:\"" + ret.expense_type + "\",expenseSubtype:\""
									+ ret.expense_subtype +"\",index:\"" + parseInt(i+1)+"\",yearName:\"" + yearName + "\",sence:\"" + sence + "\",version:\"" + version + "\",model:\"" + page_model + "\"},";
						} 
					}
					//var end = new Date().getTime();
					//alert("标栏时间："+(end-start)+"毫秒");
					 jsonArr = jsonArr.substring(0, jsonArr.lastIndexOf(",")) + "]";
					if(jsonArr!="]"){
						showMaskLayer();
						setTimeout("changeRed('"+jsonArr+"')",1);
					} 
				}
				noResultMessage("userTable");
			},
			onCellSelect : function(rowid, index, contents, event) {//当点击单元格时触发行选中
				if(rowid==201301||rowid==201302) return;//判断合计行
				//alert("cell");
				$('#userTable').jqGrid('setSelection', rowid, false);
				//汽车 项目1 项目2 只读
				if((page_model!= "AUTO" &&(index==7||index==6||index==48))||(page_model== "AUTO" &&index==48)){
					
					
					$('#userTable').jqGrid('editRow', rowid, false);
					var project_1 = ''+rowid+'_project1';
					var project_2 = ''+rowid+'_project2';
					var remark_1 = ''+rowid+'_remark';
					$('#'+project_1).change(function(){
				        //alert($(this).val());  //弹出select的值
				        var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_'));
				        if(!checkRowStatus(row_id)){
				        	$('#userTable').jqGrid('setSelection', row_id, false);
				        }
				    });
					$('#'+project_2).change(function(){
				        //alert($(this).val());  //弹出select的值
						var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_'));
				        if(!checkRowStatus(row_id)){
				        	$('#userTable').jqGrid('setSelection', row_id, false);
				        }
				    });
					$('#'+remark_1).change(function(){
				        //alert($(this).val());  //弹出select的值
						var row_id = $(this).attr('id').substring(0,$(this).attr('id').indexOf('_'));
				        if(!checkRowStatus(row_id)){
				        	$('#userTable').jqGrid('setSelection', row_id, false);
				        }
				    });
					if(index==7||index==6){
						//自动提示初始化
						initAutoCompletion();
					    //自动提示
						autoCompletion($("#"+rowid),"y",rowid);
					}
				}else if(index==1){
					$('#userTable').jqGrid('setSelection', rowid, false);
				}
			},
			onSelectRow : function(rowid) {
				//alert("row");
				if(checkRowStatus(rowid)){
					var prNum = jQuery("#userTable").jqGrid('getRowData',rowid).purchase_number;//选中行号的pr单号
					var samePrs=[];//存放所有的ids
					var dataIds = jQuery('#userTable').jqGrid('getDataIDs');
					for ( var i = 0; i < dataIds.length; i++) {//获取所有相同pr单号的，row列
						//排除 对本页合计行 与 查询结果合计行  操作
						if(dataIds[i]==201301||dataIds[i]==201302){
							continue;
						}
						if(rowid==dataIds[i])continue;//去掉自己
						var evePrNum = jQuery("#userTable").jqGrid('getRowData', dataIds[i]).purchase_number;
						if(evePrNum==prNum){
							samePrs.push(dataIds[i]);
						}
					}
					for(var i = 0; i < samePrs.length; i++){
						if(!checkRowStatus(samePrs[i])){
							$("#userTable").jqGrid('setSelection',samePrs[i],false);//设置不刷新。重要
						}
					}
				}
			}
		});
		jQuery("#userTable").jqGrid('setFrozenColumns');
	}
	
	function changeRed(jsonArr){
		 try{
		$.post("${ctx}/pr/cellDiff?t="+new Date(),"jsonArrStr=" + encodeURIComponent(jsonArr),function(data, state) {
			//var start = new Date().getTime();
			 //alert(data.value);
			 $.each(data,function(key,value){
			    	 $.each(value,function(n,v){
				    	// alert(key+"--"+v);
			    		$("tr[id="+key+"]>td[aria-describedby='userTable_"+v+"']").css("color","#FF0000");
				     });
	 			}); 
			 //var end = new Date().getTime();
			 //alert("标红时间："+(end-start)+"毫秒");
			 closeMaskLayer();
		},"json");
       }catch(e){}
	}
	// 生成合计内容
	function generateSumDataForPr(data){
		if(data.rows.length == 0)
			return;
		var data_1 ={'index':"",
	            'approval_date':"<b>本页合计</b>",
	             'purchase_number':"",
	             'purchase_name':"",
	             'project1':"",
	             'project2':"",
	             'vendor_name':"",
	             'dept':"",
	             'bizline':"",
	             'expense_type':"",
	             'expense_subtype':"",
	             'purchase_amount':data.page_pur,
	             'service_begin_date':"",
	             'service_end_date':"",
	             'user_name':"",
	             'approver_desc':"",
	             'last_oct':data.page_last10,
	             'last_nov':data.page_last11,
	             'last_dec':data.page_last12,
	             'last_year':data.page_lastTotal,
	             'jan':data.page_this01,
		         'feb':data.page_this02,
		         'mar':data.page_this03,
		         'apr':data.page_this04,
		         'may':data.page_this05,
		         'jun':data.page_this06,
		         'jul':data.page_this07,
		         'aug':data.page_this08,
		         'sep':data.page_this09,
		         'oct':data.page_this10,
		         'nov':data.page_this11,
		         'dec':data.page_this12,
		         'year':data.page_thisTotal,
	             'next_jan':data.page_next01,
	             'next_feb':data.page_next02,
	             'next_mar':data.page_next03,
	             'next_apr':data.page_next04,
	             'next_may':data.page_next05,
	             'next_jun':data.page_next06,
	             'next_jul':data.page_next07,
	             'next_aug':data.page_next08,
	             'next_sep':data.page_next09,
	             'next_oct':data.page_next10,
	             'next_nov':data.page_next11,
	             'next_dec':data.page_next12,
	             'next_year':data.page_nextTotal,
	             'remark':"",
	             'flag':""}
		
		var data_2 ={'index':"",
	            'approval_date':"<b>查询结果合计</b>",
	             'purchase_number':"",
	             'purchase_name':"",
	             'project1':"",
	             'project2':"",
	             'vendor_name':"",
	             'dept':"",
	             'bizline':"",
	             'expense_type':"",
	             'expense_subtype':"",
	             'purchase_amount':data.total_pur,
	             'service_begin_date':"",
	             'service_end_date':"",
	             'user_name':"",
	             'approver_desc':"",
	             'last_oct':data.total_last10,
	             'last_nov':data.total_last11,
	             'last_dec':data.total_last12,
	             'last_year':data.total_lastTotal,
	             'jan':data.total_this01,
		         'feb':data.total_this02,
		         'mar':data.total_this03,
		         'apr':data.total_this04,
		         'may':data.total_this05,
		         'jun':data.total_this06,
		         'jul':data.total_this07,
		         'aug':data.total_this08,
		         'sep':data.total_this09,
		         'oct':data.total_this10,
		         'nov':data.total_this11,
		         'dec':data.total_this12,
		         'year':data.total_thisTotal,
	             'next_jan':data.total_next01,
	             'next_feb':data.total_next02,
	             'next_mar':data.total_next03,
	             'next_apr':data.total_next04,
	             'next_may':data.total_next05,
	             'next_jun':data.total_next06,
	             'next_jul':data.total_next07,
	             'next_aug':data.total_next08,
	             'next_sep':data.total_next09,
	             'next_oct':data.total_next10,
	             'next_nov':data.total_next11,
	             'next_dec':data.total_next12,
	             'next_year':data.total_nextTotal,
	             'remark':"",
	             'flag':""}
        jQuery('#userTable').jqGrid('addRowData', 201301, data_1, '');
		jQuery('#userTable').jqGrid('addRowData', 201302, data_2, '');
		
		$("tr[id=201301]>td:lt(2)", $("#userTable")).html("");
		$("tr[id=201301]>td:lt(2)", $("#userTable_frozen")).html("");
		
		$("tr[id=201302]>td:lt(2)", $("#userTable")).html("");
		$("tr[id=201302]>td:lt(2)", $("#userTable_frozen")).html("");
	}
</script>
</head>
<body>
<div style="display:none">
	<s:select id="project1_Auto" list="project1Map"  headerKey="" headerValue="请选择"/>
	<s:select id="project2_Auto" list="project2Map"  headerKey="" headerValue="请选择"/>
</div>
<div class="mainCont">
	<div class="mainRight">
		<div class="mTitleBig">
			<h3>
				<span><s:property value="templateName" />_PR修改数据</span>
				<s:if test="template.temTip != null && template.temTip != ''"><a title="查看操作提示" href="${ctx}/commontemplate/showhelp?templateCode=${template.temCode}" target="_blank"><span class="tipSpan"></span></a>
				</s:if>
			</h3>
			<div class="clear">&nbsp;</div>
		</div>
<form id="xForm" method="post" name="xForm" action="${ctx}/pr/export?viewName=y&model=<s:property value='#request.model'/>">
    <input  id="bg_viewName"  value="y" type="hidden" />
	<input  id="bg_model" name="prListBuluQuery.model" value="<s:property value='#request.model'/>" type="hidden" />
		<div id="div_bg" class="mRcont">
			<div class="mTitle">
				<h4>年度、场景、版本选择</h4>
				<div class="mRsearchGen">
					<span>年：</span>
					<s:select id="bg_year" list="{2013,2014}" name="prListBuluQuery.yearName"></s:select>
					<span>场景：</span>
					<s:select id="bg_sence" list="{'Q1Fcst', 'Q1Latest', 'Q2Fcst', 'Q2Latest', 'Q3Fcst', 'Q3Latest', 'Q4Fcst', 'Q4Latest', 'AnnualBudget', 'Actual'}" name="prListBuluQuery.sence"></s:select>
					<span>版本：</span>
					<s:select id="bg_version" list="{'V1', 'V2', 'V3', 'Final', 'Test'}" name="prListBuluQuery.version"></s:select>
					<span>状态：</span> 
					<span class="mRMap" id="span_bg_allow">${prListBuluQuery.allow == 0 ? '已锁定' : (prListBuluQuery.allow == 1 ? '可录入' : '未开启')}</span>
					<span id="span_bg_allow_pic" class="${prListBuluQuery.allow == 0 ? 'allow0' : (prListBuluQuery.allow == 1 ? 'allow1' : 'allow2')}"></span>
					<input type="hidden" id="canWrite" value="${prListBuluQuery.allow}" />
					<a id="switchBgMapping" class="subUse subBtn" href="javascript:void(0);" onclick="switchBgMapping(false,'<s:property value="#request.model"/>','x',ctx);">切换</a>
				</div>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
			<div id="div_search">
				<div class="mTitle" style="background: none;">
					<h4>
						查询条件选择&nbsp;
						<a href="javascript:toggleSearchArea();">显示/折叠查询区域</a>
					</h4>
					<div class="clear">&nbsp;</div>
				</div>
				<div class="mRsearch" id="div_search_cond" style="display: none">
					<table style="width:100%">
							<tr>
								<td class="mRsTitle" id="bizlineTd">业务线：</td>
								<td id="bizlineTd2">
									<s:select multiple="true" id="search_BUSINESS_LINE" name="prLineSelect" list="bizLineMap.get(#request.prLine)" listKey="llCode" listValue="llName"></s:select>
									<s:select id="search_BUSINESS_LINE_hidden_" list="bizLineMap.get(#request.prLine)" listKey="%{llParentCode + '@' + llCode}" listValue="llName" cssStyle="display:none;"></s:select>
								</td>
								<td class="mRsTitle">费用大类：</td>
								<td>
									<s:select multiple="true" id="search_EXPENSE_TYPE" name="prTypeSelect" list="expenseTypeMap.get(#request.expenseType)" listKey="llCode" listValue="llName"></s:select>
									<s:select id="search_EXPENSE_TYPE_hidden_BUSINESS_LINE" list="expenseTypeMap.get(#request.expenseType)" listKey="%{llParentCode + '@' + llCode}" listValue="llName" cssStyle="display:none;"></s:select>
								</td>
								<td class="mRsTitle">费用小类：</td>
								<td>
									<s:select multiple="true" id="search_EXPENSE_SUBTYPE" name="prSubTypeSelect" list="expenseSubTypeMap.get(#request.subExpenseType)" listKey="llCode" listValue="llName"></s:select>
									<s:select id="search_EXPENSE_SUBTYPE_hidden_EXPENSE_TYPE" list="expenseSubTypeMap.get(#request.subExpenseType)" listKey="%{llParentCode + '@' + llCode}" listValue="llName" cssStyle="display:none;"></s:select>
								</td>
							</tr>
							<tr>
								<td class="mRsTitle">PR单号：</td>
								<td>
									<input type="text" name="prListBuluQuery.purchaseNumber" />
								</td>
								<td class="mRsTitle">项目1：</td>
								<td>
									<s:select style="width:200px" name="prListBuluQuery.project1" id="project1" list="project1Map"  headerKey="" headerValue="请选择"/>
								</td>
								<td class="mRsTitle">项目2：</td>
								<td>
									<s:select style="width:200px" name="prListBuluQuery.project2" id="project2" list="project2Map"  headerKey="" headerValue="请选择"/>
								</td>
								
							</tr>
						</table>
						<input id="prLine" type="hidden" name="prListBuluQuery.prLine" value=""/>
						<input id="prType" type="hidden" name="prListBuluQuery.prType" value=""/>
						<input id="prSubType" type="hidden" name="prListBuluQuery.prSubType" value=""/>
				</div>
			</div>
				<input id="reset" type="reset" style="display: none" />
		</form>

		<div id="div_sub" class="seaSub">
			<a id="queryBtn" class="subUse subBtn" href="javascript:void(0);" onclick="check('query');return false;">查询</a>
		
			<a class="subRestBtn" href="javascript:void(0);" onclick="resetxForm('<s:property value='#request.model'/>');">重置</a>
		</div>
		<div class="JgopDes">
			<div id="div_summary" class="jGdesc">
				<strong>共计数据条数：</strong> <span class="numSp" id="count"></span> <strong>与PR系统同步时间：</strong> <span class="timeSp" id="syncDate"></span> <strong>最后修改人：</strong> <span class="editSp" id="syncPeople"></span>
				<span><span style="color:#FF0000">红色</span>：PR系统变更数据&nbsp;&nbsp;&nbsp;</span>
				<span><span style="color:#0000FF">蓝色</span>：PR系统已删除</span>
			</div>
			<div id="div_summary" class="mRsarOp">
				<a id="syncBtn" href="javascript:void(0)" class="geyBtn" onclick="check('syncDate');return false;">数据同步</a>
				<!--    <a id="commitBtn" href="javascript:void(0)" class="geyBtn3">提交数据</a>  -->
				<a id="saveBtn" href="javascript:void(0)" class="geyBtn2" onclick="check('saveBtn');return false;">保 存</a>
				<a id="importBtn" href="javascript:void(0)" class="geyBtn" onclick="check('importExcel');return false;">从Excel导入</a>
				<a id="exportBtn" href="javascript:void(0)" class="geyBtn" onclick="check('exportExcel');return false;">导出Excel</a>
			</div>
		</div>

		<!-- JG列表 -->
		<div class="jGlist" z-index="1">
			<table id="userTable" class="userTable"></table>
		</div>
		<!-- 分页 -->
		<div id="jGpage"></div>
	</div>
	<div class="clear">&nbsp;</div>
</div>
<%@ include file="pr-excel.jsp"%>
</body>
</html>