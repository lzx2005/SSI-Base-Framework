/**
 * 绑定按钮事件，触发异步提交搜索form
 *
 * @param buttonId     按钮ID
 * @param formId       表单ID
 * @param jqGridId     JqGrid的table的id
 * @param urlStr       请求路径
 * @param beforeSubmit 提交之前需要调用的函数
 */
function bindSearchClick(buttonId, formId, jqGridId, urlStr, beforeSubmit) {
	jQuery("#" + buttonId).click(function() {
		jQuery("#" + jqGridId).jqGrid('setGridParam',{postData :null});//清空postData
		if(beforeSubmit){
			var flag = beforeSubmit();
			if(flag == false)
				return;
		}
		jQuery("#" + jqGridId).jqGrid('setGridParam', {
			url : urlStr,
			datatype : "json",
			mtype : "POST",
			postData : $("#" + formId).serializeObject(),
			autoencode : false,
			altRows : true,
			page : 1
		}).trigger("reloadGrid");
	});
}

/**
 * 重置表单的搜索条件
 * 
 * @param divSearchId 搜索条件所在div的Id
 */
function resetForm(divSearchId){
	var parent = $("#" + divSearchId);
  $("input[type=checkbox]", parent).attr("checked", false);
  $("input[type=text]", parent).val("");
  $("input[type=hidden]", parent).val("");
  $("select", parent).each(function(){
  	//alert($(this).parent().val());
  	// alert($(this).val());
  });
  $("select>option", parent).removeAttr("selected");
  $("select", parent).val("");
  $("textarea", parent).val("");
}

/**
 * 点击回车键执行搜索
 * 
 * @param btnId
 */
function typeSubmit(btnId) {
	$(document).keyup(function(event) {
		if (event.keyCode == 13) {
			$("#" + btnId).trigger("click");
		}
	});
}


/**
 * 无查询结果时，给出友好提示信息
 * 
 * @param jqGridId
 */
function noResultMessage(jqGridId) {
	var re_records = $("#" + jqGridId).getGridParam('records');
	var row = $("#" + jqGridId).getGridParam("reccount");
	if (row == 0) {
		var messagelist = $("#messagelist" + jqGridId);
		if (messagelist) {
			messagelist.remove();
		}
		$("#" + jqGridId).parent().append(
			"<div id='messagelist" + jqGridId + "' style='text-align:center; padding:10px 0; width：100%;font-size:12px;' align='left'>没搜索到数据</div>");
		var lastTh = $("#" + jqGridId).parent().parent().parent().find(".ui-th-column").last();
		$(lastTh).css("width", $(lastTh).width());
	} else {
		$("#messagelist" + jqGridId).hide();
	}
}

// 序列化表单
$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

var global_left_width = 232;
// 自适应数据表格宽度和高度
function resizeGrid(jqGridId, leftw){
	var docHeight = $(window.parent.document).height();//浏览器窗口的高度
	if(leftw || leftw == 0) global_left_width = leftw;
	// var leftWidth = window.parent.parent.AllFrame.cols;
	// leftWidth = parseInt(leftWidth.split(',')[0]);
	var docWidth = $(window.parent.document).width() - 10 - 8 - global_left_width; // 浏览器窗口的宽度
	var tookHeight = 19;// 一些留白高度
	$(".mainRight>div").each(function(i, o){
		if($(this).attr("class") == 'jGlist'){
			tookHeight += 0;
		} else {
			tookHeight += $(this).height();
		}
	});
	$("form>div").each(function(i, o){
		tookHeight += $(this).height();
	});
	// 去除分页条的高度
	if($("#jGpage"))
		tookHeight += $("#jGpage").height() + 1;
	tookHeight += $(".jGlist thead").height();
	try{
		$("#" + jqGridId).jqGrid('setGridHeight', docHeight - tookHeight);
		$("#" + jqGridId).jqGrid('setGridWidth', docWidth);
	} catch(e){}
}

/** 显示遮罩层 */
function showMaskLayer() {
	$("body:eq(0)").append('<div class="MaskLayer"></div>');
	$("body:eq(0)").append('<div class="loadImg"></div>');
	resetLoadImage();
}
/** 移除遮罩层 */
function closeMaskLayer() {
	$(".MaskLayer").remove();
	$(".loadImg").remove();
}
/** 刷新图片的位置 */
function resetLoadImage() {
	var HALF_BODY_WIDTH = document.body.offsetWidth / 2;
	var HALF_BODY_HEIGHT = document.body.offsetHeight / 2;
	$(".loadImg").css("left", (HALF_BODY_WIDTH - 110) + "px");
	if ($("#boxTop").length == 1) {
		$(".loadImg").css("bottom", "200px");
	} else {
		$(".loadImg").css("top", (HALF_BODY_HEIGHT - 9.5) + "px");
	}
}