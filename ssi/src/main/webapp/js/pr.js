    /**
     * 保存 切换前的页面值
     */
    var page_year = "";
    var page_sence = "";
    var page_version = "";
    var page_model = "";
    var page_viewName = "";
    
    var page_project1 = "";
    var page_project2 = "";
    
    var page_bizLine = "";
    var page_type = "";
    var page_subType = "";
    
    var project1Arr=[];
    var project2Arr=[];
    
    var isExitDel = false;//判断是否存在删除数据  false 不存在
    var jsonArr_del = '';//待删除数据
    //自动提示初始化
    function initAutoCompletion(){
    	//alert("project1Arr="+project1Arr);alert("project2Arr="+project2Arr);
    	if(project1Arr.length != 0 ||project2Arr.length != 0)return false;
    	//project1Arr=[];
    	//project2Arr=[];
    	$("#project1_Auto").find("option").each(function(){
			if($(this).text() != '请选择')
				project1Arr.push($(this).text());
		});
    	$("#project2_Auto").find("option").each(function(){
    		if($(this).text() != '请选择')
    			project2Arr.push($(this).text());
    	});
    	//alert("project1Arr="+project1Arr);alert("project2Arr="+project2Arr);
    }
  //自动提示
    function autoCompletion(tr,viewName,row_id){
    	var input_text1 ; 
        var input_text2 ;
        var old_val = "";
        var new_val = "";
    	if(viewName=='z'){
    		 input_text1 = tr.find("input[name=projectName1]");
             input_text2 = tr.find("input[name=projectName2]");
    	}else{
    		 input_text1 = tr.find("input[name=project1]");;
             input_text2 = tr.find("input[name=project2]");
    	}
		input_text1.autocomplete({
			source:project1Arr,
			select:function(event){
				old_val = $(this).val();
			},
			close:function(event){
				new_val = $(this).val();
				if(old_val!=new_val){
					if(!checkRowStatus(row_id)){
			        	$('#userTable').jqGrid('setSelection', row_id, false);
			        }
				}
			}
	    });
		input_text2.autocomplete({
			source:project2Arr,
			select:function(event){
				old_val = $(this).val();
			},
			close:function(event){
				new_val = $(this).val();
				if(old_val!=new_val){
					if(!checkRowStatus(row_id)){
			        	$('#userTable').jqGrid('setSelection', row_id, false);
			        }
				}
			}
	    });
    }
   /**
	 * 重置
	 */
	function resetxForm(obj) {
		$("#reset").click();
		$("#xForm").resetForm();
		//解决不同浏览器版本，select(项目1，项目2)重置问题
		if(document.getElementById("project1")&&document.getElementById("project2")){
			document.getElementById("project1").selectedIndex=0;
			document.getElementById("project2").selectedIndex=0;
			$("#project1  option[text='请选择'] ").attr("selected",true);
			$("#project2  option[text='请选择'] ").attr("selected",true);
		}
		
		changeType(obj);
		//还原年场景版本
		backVersion();
	}
	/**
	 * 刷新级联
	 */
	function changeType(obj){
		//
		if(obj=='SOHU'){
			$("#search_BUSINESS_LINE").find("option[value='SOHU_BIZLINE01']").attr("selected", true);
		}
		else{
			$("#search_BUSINESS_LINE").find("option[value!='']").attr("selected", true);
		}
		initMultiSelect($("select[multiple=multiple]"));
		
		if(obj=='FOCUS'){
			$("#search_EXPENSE_TYPE").find("option[value!='']").attr("selected", true);
			$("#search_EXPENSE_TYPE").multiselect("close");
			$("#search_EXPENSE_TYPE").multiselect("refresh");
		} 
	}
	/**
	 * excel导出
	 */
	function exportExcel() {
		//加上排序功能
		var sortname = jQuery("#userTable").jqGrid('getGridParam', 'sortname');
		var sortorder = jQuery("#userTable").jqGrid('getGridParam', 'sortorder');
		var action = $("#xForm").attr("action");
		var new_action  = action+"&sortname="+sortname+"&sortorder="+sortorder;
		
		$("#xForm").attr("action", new_action);
		$("#xForm").submit();
		$("#xForm").attr("action", action);
	}
	//显示/折叠查询区域
	function toggleSearchArea(){
		if($("#div_search_cond").css("display") == 'none')
			$("#div_search_cond").show();
		else
			$("#div_search_cond").hide();
		resizeGrid("userTable");
	}
	//切换年度场景版本
	function switchBgMapping(flag,model,viewName,ctx) {
		projcectValue_select();////保存原project值
		saveOldLookUpValue();//保存old 大小类
		var url=ctx+"/commontemplate/switchBgMapping";
		$.post(url, {
			'bgMapping.yearName' : $("#bg_year").val(),
			'bgMapping.sence' : $("#bg_sence").val(),
			'bgMapping.version' : $("#bg_version").val(),
			'bgMapping.section' : model
		}, function(data) {
			$("#span_bg_allow").html(data == 0 ? "已锁定" : (data == 1 ? "可录入" : "未开启"));
			$("#canWrite").val(data);
			//改变场景版本状态图标
			$("#span_bg_allow_pic").removeClass();
			if(data==0){
				$("#span_bg_allow_pic").addClass("allow0");
			}
			else if(data==1){
				$("#span_bg_allow_pic").addClass("allow1");
			}
			else{
				$("#span_bg_allow_pic").addClass("allow2");
			}
			
			optionNone();//设定操作按钮状态
			//版本是未开启状态时，不显示修改人、修改时间
			hiddenUpdateUserAndTime();
			initPageBgMapping();//设置原场景、版本、年
			//自动提示 项目1 2 值集
			changeProjectForAuto();
			project1Arr=[];
	    	project2Arr=[];
			/*if (!flag)
				alert("切换成功!");*/
			//重新请求业务线 大小类值集
			refreshLookUp();
			
		}, 'json');
	}
	//切换年场景版本时，刷新业务线  大小类值集
	function refreshLookUp(){
		var url_2=ctx+"/pr/changValue?model="+page_model;
		$.post(
				url_2,
				function(data){
					var bizLine_1_arr = data.bizLine_1.split(',');
					var bizLine_2_arr = data.bizLine_2.split(',');
					var expenseType_1_arr = data.expenseType_1.split(',');
					var expenseType_2_arr = data.expenseType_2.split(',');
					var expenseSubType_1_arr = data.expenseSubType_1.split(',');
					var expenseSubType_2_arr = data.expenseSubType_2.split(',');
					
					$("#search_BUSINESS_LINE").empty();
					$("#search_BUSINESS_LINE_hidden_").empty();
					$("#search_EXPENSE_TYPE").empty();
					$("#search_EXPENSE_TYPE_hidden_BUSINESS_LINE").empty();
					$("#search_EXPENSE_SUBTYPE").empty();
					$("#search_EXPENSE_SUBTYPE_hidden_EXPENSE_TYPE").empty();
					
					if(data.bizLine_1!=""){
						createSelect(bizLine_1_arr,'search_BUSINESS_LINE');
					}
					
					if(data.bizLine_2!=""){
						createSelect(bizLine_2_arr,'search_BUSINESS_LINE_hidden_');
					}
					
					if(data.expenseType_1!=""){
						createSelect(expenseType_1_arr,'search_EXPENSE_TYPE');
					}
					
					if(data.expenseType_2!=""){
						createSelect(expenseType_2_arr,'search_EXPENSE_TYPE_hidden_BUSINESS_LINE');
					}
					
					if(data.expenseSubType_1!=""){
						createSelect(expenseSubType_1_arr,'search_EXPENSE_SUBTYPE');
					}
					
					if(data.expenseSubType_2!=""){
						createSelect(expenseSubType_2_arr,'search_EXPENSE_SUBTYPE_hidden_EXPENSE_TYPE');
					}
					set_selectValue('search_BUSINESS_LINE',page_bizLine);
					$("#search_BUSINESS_LINE").multiselect("close");
					$("#search_BUSINESS_LINE").multiselect("refresh");
					
					set_selectValue('search_EXPENSE_TYPE',page_type);
					$("#search_EXPENSE_TYPE").multiselect("close");
					$("#search_EXPENSE_TYPE").multiselect("refresh");
					
					set_selectValue('search_EXPENSE_SUBTYPE',page_subType);
					$("#search_EXPENSE_SUBTYPE").multiselect("close");
					$("#search_EXPENSE_SUBTYPE").multiselect("refresh");
					// 刷新文本框自动完成功能的候选项
					if(page_viewName=="z"||page_viewName=="y"){
						changeProjectForOption(true);
					}
					else{
						$("#queryBtn").trigger("click");
					}
				},
				'json'
		);
	}

	//切换年场景版本时，刷新autocomplate功能的候选项
	function changeProjectForOption(isQuery){
		if(page_viewName!='y'&&page_viewName!='z'){
			return;
		}
		var params = $("#xForm").serialize();
		var url=ctx+"/pr/queryProject?viewName="+page_viewName+"&" + params;
		$.post(
				url,
				function(data){
					var arr1 = data.project1.split(',');
					var arr2 = data.project2.split(',');
					
					$("#project1").empty();
					$("#project2").empty();
					
					$("#project1").append("<option value='' selected>请选择</option>");
					if(data.project1!=""){
						for(var i=0;i<arr1.length;i++){
							var option = "<option value='"+arr1[i]+"'>"+arr1[i]+"</option>";
							$("#project1").append(option);
						}
					}
					
					$("#project2").append("<option value=''>请选择</option>");
					if(data.project2!=""){
						for(var i=0;i<arr2.length;i++){
							var option = "<option value='"+arr2[i]+"'>"+arr2[i]+"</option>";
							$("#project2").append(option);
						}
					}
					
					defaultValue_select();
					
					initPageBgMapping();
					
					projcectValue_select();
					
					if(isQuery){
						$("#queryBtn").trigger("click");
					}
					
				},
				'json'
		);
	}
	
	// 设置年场景版本的值到页面变量中
	function initPageBgMapping(){
		page_year = $("#bg_year").val();
	    page_sence = $("#bg_sence").val();
	    page_version= $("#bg_version").val();
	    page_model = $("#bg_model").val();
	    page_viewName = $("#bg_viewName").val();
	}
	//判断场景版本是否切换  false 未改变 true 改变
	function isChangeVersion(){
		var src = ""+page_year+page_sence+page_version;
		var tr = ""+$("#bg_year").val()+$("#bg_sence").val()+$("#bg_version").val();
		//alert(src.length>0);alert(src);alert(tr);alert(src!=tr);
		if(src.length>0&&src!=tr){
			return true;
		}
		return false;
	}
	//获取select值
	function selectValue(id){
		var text = '';
		$("#"+id).find("option:selected").each(function(){
		    text += $(this).val() + ',';
		});
		if(text!=''){
			text = text.substring(0, text.lastIndexOf(','));
		}
		return text;
	}
	//保存获取select值
	function set_selectValue(id,val){
		var arr = val.split(",");
		$("#"+id).children().each(function(){
			for(var i=0;i<arr.length;i++){
				if($(this).val()==arr[i]){
					$(this).attr("selected","selected");
					break;
				}
			}
		    
		});
	}
	//保存原project值
	function projcectValue_select(){
		 page_project1 = selectValue("project1");
		 page_project2 = selectValue("project2");
	}
	//修改select默认值
	function defaultValue_select(){
		 set_selectValue("project1",page_project1);
		 set_selectValue("project2",page_project2);
	}
	//还原年场景版本
	function backVersion(){
		set_selectValue("bg_year",page_year);
		set_selectValue("bg_sence",page_sence);
		set_selectValue("bg_version",page_version);
	}
	
	/**
	 * 按钮不可用  所有不可用  同步
	 */
	function disableBtn() {
		$("#syncBtn,#commitBtn,#delBtn,#saveBtn,#importBtn,#exportBtn").attr("disabled","disabled");
		$("#syncBtn,#commitBtn,#delBtn,#saveBtn,#importBtn,#exportBtn").addClass("disabledBtn");
	}
	/**
	 * 按钮可用 所有可用  同步
	 */
	function normalBtn() {
		$("#syncBtn,#commitBtn,#delBtn,#saveBtn,#importBtn,#exportBtn").removeAttr("disabled"); 
		$("#syncBtn,#commitBtn,#delBtn,#saveBtn,#importBtn,#exportBtn").removeClass("disabledBtn");
	}
	/**
	 * 按钮不可用  导出可用，其它不可用
	 */
	function disableBtn2() {
		$("#syncBtn,#commitBtn,#delBtn,#saveBtn,#importBtn").attr("disabled","disabled");
		$("#syncBtn,#commitBtn,#delBtn,#saveBtn,#importBtn").addClass("disabledBtn");
	}
	
	/**
	 * 根据状态 锁定按钮
	 */
	function optionNone(){
		var allow = $("#canWrite").val();
		if(allow!='1'){
			disableBtn2();
		}
		else{
			normalBtn();
		}
	}
	/**
	 * 保存原值集 业务线、大小类
	 */
	function saveOldLookUpValue(){
		 page_bizLine = selectValue("search_BUSINESS_LINE");
		 page_type =  selectValue("search_EXPENSE_TYPE");
		 page_subType =  selectValue("search_EXPENSE_SUBTYPE");
	}
		
	/**
	 * 创建select
	 */
	function createSelect(arr,id){
		for(var i=0;i<arr.length;i++){
			var content = arr[i];
			var key = content.substring(0,content.indexOf('-'));
			var val = content.substring(content.indexOf('-')+1);
			var option = "<option value='"+key+"'>"+val+"</option>";
			$("#"+id).append(option);
		}
	}
	/**
	 * 保存 业务线 费用大小类值 查询使用
	 */
	function setTypeForQuery(){
		 $("#prLine").val(selectValue("search_BUSINESS_LINE"));
		 $("#prType").val(selectValue("search_EXPENSE_TYPE"));
		 $("#prSubType").val(selectValue("search_EXPENSE_SUBTYPE"));
	}
	/**
	 * 版本是未开启状态时，不显示修改人、修改时间
	 */
	function hiddenUpdateUserAndTime(){
		var allow = $("#canWrite").val();
		if(allow=='2'){//未开启
			$("#syncDate").css('display','none');
			$("#syncPeople").css('display','none');
		}
		else{
			$("#syncDate").css('display','block');
			$("#syncPeople").css('display','block');
		}
	}
	/**
	 * 数据同步 提示信息
	 */
	function sycnMsgDialog(user,option){
		var msg = '';
		if(option=='init'){
			msg = '操作用户：'+user+'\n\n'+'正在同步中,同步，提交数据，保存等操作不可用！';
		}
		else if(option=='prlist_sync'){
			msg = '操作用户：'+user+'\n\n'+'PrList同步正在进行中，请稍候再操作！';
		}
		else if(option=='videoCopy_sync'){
			msg = '操作用户：'+user+'\n\n'+'视频版权同步正在进行中，请稍候再操作！';
		}
		else{
			msg = '操作用户：'+user+'\n\n'+'正在执行同步请等候。。。';
		}
		alert(msg);
	}
	//检查是否能跳转 true 是可以跳转  false 加入判断选择是的时候可以跳转
	function checkGoOtherPage(){
		if(!isExitDel&&!isExitSaveOption()){
			return true;
		}else{
			return false;
		}
	}
	//是否包含修改保存项  true 存在 false 不存在
	function isExitSaveOption(){
		var selectedIds = jQuery("#userTable").jqGrid('getGridParam', 'selarrrow');
		var editRows = 0;
		for ( var i = 0; i < selectedIds.length; i++) {
			if(selectedIds[i]==201301||selectedIds[i]==201302){
				continue;
			} 
			editRows++;
		}
		if(editRows==0){
			return false;
		}
		return true;
	}
	//自动提示 项目1 2 值集
	function changeProjectForAuto(){
		if(page_viewName!='x'&&page_viewName!='y'&&page_viewName!='z'){
			return;
		}
		var params = $("#xForm").serialize();
		var url=ctx+"/pr/queryProjectAuto?viewName="+page_viewName+"&" + params;
		$.post(
				url,
				function(data){
					var arr1 = data.project1.split(',');
					var arr2 = data.project2.split(',');
					//alert(data.project1);alert(data.project2);
					$("#project1_Auto").empty();
					$("#project2_Auto").empty();
					
					$("#project1_Auto").append("<option value='' selected>请选择</option>");
					if(data.project1!=""){
						for(var i=0;i<arr1.length;i++){
							var option = "<option value='"+arr1[i]+"'>"+arr1[i]+"</option>";
							$("#project1_Auto").append(option);
						}
					}
					
					$("#project2_Auto").append("<option value=''>请选择</option>");
					if(data.project2!=""){
						for(var i=0;i<arr2.length;i++){
							var option = "<option value='"+arr2[i]+"'>"+arr2[i]+"</option>";
							$("#project2_Auto").append(option);
						}
					}
					
				},
				'json'
		);
	}
	//验证当前行 选中状态  true 选中
	function checkRowStatus(rowId){
		var selrows = jQuery("#userTable").jqGrid('getGridParam', 'selarrrow');
		for(var i=0;i<selrows.length;i++){
			if(selrows[i]==201301||selrows[i]==201302){
				continue;
			} 
			if(selrows[i]==rowId){
				return true;
			} 
		}
		return false;
	}