/**JG列表扩展方法***/
/**分页**/
function generationPaging(jqGridId, pagingId){
	var pageId = $('#' + pagingId);
	var jGid = $('#' + jqGridId);
	var page = jGid.jqGrid('getGridParam', 'page');// 当前页
	var rowNum = jGid.jqGrid('getGridParam', 'rowNum');// 每页显示的最大条数
	var records = jGid.jqGrid('getGridParam', 'records');// 总共多少条数
	var maxPage;// 总页数
	if (records && records > 0) {
		maxPage = parseInt((parseInt(records) + parseInt(rowNum) - 1)/ parseInt(rowNum));
	}
	var pageHtml = ['<div class="jGpage">',
						'<a class="homePage" href="###">首页</a>',
						'<a class="prePage" href="###">上一页</a>',
						'<a class="nextPage" href="###">下一页</a>',
						'<a class="lastPage" href="###">末页</a>',
						'<span>共'+maxPage+'页</span>',
						'<input type="text" value="1"/>',
						'<a class="turnPage" href="###" title="跳转">&nbsp;</a>',
					'</div>'];
	pageId.html(pageHtml.join(""));
	var homePage = pageId.find(".homePage:eq(0)");//首页按钮
	var prePage = pageId.find(".prePage:eq(0)");//上一页按钮
	var nextPage = pageId.find(".nextPage:eq(0)");//下一页按钮
	var lastPage = pageId.find(".lastPage:eq(0)");//末页按钮
	var totalPage = pageId.find("span:eq(0)");//总页按钮
	var editPage = pageId.find("input:eq(0)");//输入框
	var ediVal;
	var turnPage = pageId.find(".turnPage:eq(0)");//跳转按钮
	editPage.val(page);
	$(homePage).off("click").on("click",function(){
		if(maxPage==1){
			return false;
		}else{
			jumpPage(jqGridId, 1);
		}
	});
	$(prePage).off("click").on("click",function(){
		page = $(jGid).jqGrid('getGridParam', 'page');
		if (page > 1) {
			jumpPage(jqGridId, page - 1);
		}else{
			return false;
		}
	});
	$(nextPage).off("click").on("click",function(){
		page = $(jGid).jqGrid('getGridParam', 'page');
		if (page < maxPage) {
			jumpPage(jqGridId, page + 1);
		}else{
			return false;
		}
	});
	$(lastPage).off("click").on("click",function(){
		if(maxPage==1){
			return false;
		}else{
			jumpPage(jqGridId, maxPage);
		}
	});
	$(editPage).off("blur").on({
		"blur":function(){
			ediVal = $(this).val();
			var reg =  /^\d+$/g;
			if(!reg.test(ediVal)){
				alert("输入不合法");
				$(this).val(page);
				return;
			}else if(ediVal>maxPage){
				alert("输入值不能大于总页数");
				$(this).val(page);
				return;
			}
		}
	});
	$(turnPage).off("click").on("click",function(){
		page = $(jGid).jqGrid('getGridParam', 'page');
		if (page == ediVal) {
			return false;
		}else{
			jumpPage(jqGridId, ediVal);
		}
	});
	initListData(jqGridId, pagingId);
}

/**指定页号**/
function jumpPage(jqGridId, pageNum) {
	jQuery("#" + jqGridId).jqGrid('setGridParam', {
		page : pageNum
	}).trigger("reloadGrid");
}
/** 无结果时，初始化数据 */
function initListData(jqGridId,pagingId) {
	var row = $("#" + jqGridId).getGridParam("reccount");
	if (row == 0) {
		var dataRow = {id:"1"};
		var tdLength = $("#" + jqGridId).find("tr:eq(0)").find("td").length;
		$("#" + jqGridId).jqGrid("addRowData", "noMessage", dataRow, "last");   
		$("#noMessage").html("<td colspan="+tdLength+" style='text-align:center'>无数据</td>");
		$("#" + pagingId).html("");
	};
}























