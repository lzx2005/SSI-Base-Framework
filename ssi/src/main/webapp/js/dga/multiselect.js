// 初始化multiselect插件
function initMultiSelect(elements){
	elements.each(function(i,o){
		$(o).multiselect({
			selectedList:100,
			close: function(e){
				// 关闭选择层时，先生成子<select>的option，在调用插件的刷新方法，实现级联的效果。
				var sub_select = changeSelect(e.target);
				if(sub_select){
					sub_select.multiselect("refresh");
					// 递归触发close事件
					sub_select.multiselect("close");
				}
	   	}
		}).multiselectfilter();
	});
	// 触发close事件
	elements.each(function(i,o){
		$(o).multiselect("close");
	});
}
// 生成子下拉框的选项
function changeSelect(obj){
	var sub_hidden = $("select[id$=_hidden_" + $(obj).attr("id").substring(7) + "]");
	if(!sub_hidden || !sub_hidden.attr("id"))
		return null;

	var sub_hidden_id = sub_hidden.attr("id");
	var sub_select = $("#search_" + sub_hidden_id.substring(7, sub_hidden_id.indexOf('_hidden_')));
	sub_select.val("");
	sub_select.find("option[value!='']").remove();
	if(obj.value){
		for(var i = 0; i < obj.options.length; i++){
			var op = obj.options[i];
			if($(op).attr("selected")){
				var new_ops = sub_hidden.find("option[value^='" + op.value + "@']").clone();
				new_ops.each(function(i, o){
					if(o.value.indexOf('@') != -1)
						o.value = o.value.substring(o.value.indexOf('@') + 1);
				});
				sub_select.append(new_ops);
			}
		}
		sub_select.val("");
	}
	return sub_select;
}
